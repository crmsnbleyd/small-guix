(define-module (small-guix system input)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:export (small-guix-kl small-guix-hid-apple-config))

(define small-guix-kl
  (keyboard-layout "us"))

(define small-guix-hid-apple-config
  (plain-file "hid_apple.conf" "options hid_apple fnmode=2 swap_opt_cmd=1"))
