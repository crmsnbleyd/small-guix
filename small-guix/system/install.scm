;;; Copyright © 2021 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Generate a bootable image (e.g. for USB sticks, etc.) with:
;; $ guix system disk-image small-guix/system/install.scm

(define-module (small-guix system install)
  #:use-module (gnu system)
  #:use-module (nongnu system install)
  #:use-module (small-guix services base)
  #:use-module (small-guix services desktop)
  #:export (sg-installation-os-base sg-installation-os-desktop))

(define sg-installation-os-base
  (operating-system
    (inherit installation-os-nonfree)
    (services
     %small-guix-base-services)))

(define sg-installation-os-desktop
  (operating-system
    (inherit installation-os-nonfree)
    (services
     %small-guix-desktop-services)))
