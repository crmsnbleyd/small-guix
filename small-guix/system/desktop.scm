;;; Copyright © 2021, 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix system desktop)
  #:use-module (gnu)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages vpn)
  #:use-module (gnu system)
  #:use-module (gnu system nss)
  #:use-module (small-guix locales)
  #:use-module (small-guix packages brillo)
  #:use-module (small-guix packages gnome)
  #:use-module (small-guix services desktop)
  #:use-module (small-guix system input)
  #:export (small-guix-desktop-system))

(define small-guix-desktop-system
  (operating-system
    (locale "en_US.utf8")
    (timezone "Europe/Rome")
    (keyboard-layout small-guix-kl)

    (bootloader (bootloader-configuration
                  (bootloader grub-efi-bootloader)
                  (targets '("/boot/efi"))
                  (keyboard-layout small-guix-kl)))

    (file-systems (list (file-system
                          (mount-point "/")
                          (device "/dev/fake")
                          (type "ext4"))))

    (host-name "small-guix")

    (users %base-user-accounts)

    (groups (append %base-groups
                    (list
                          ;; This group is required by ddcutil.
                          (user-group
                            (system? #t)
                            (name "i2c"))
                          ;; This group is required by some real time applications,
                          ;; such as SuperCollider.
                          (user-group
                            (system? #t)
                            (name "realtime")))))

    (packages (append (list gnome-browser-connector brillo small-guix-glibc-locales)
                      (map specification->package+output
                           (list
                                 ;; Pulseaudio
                                 "pulseaudio"
                                 "pavucontrol"

                                 "wireguard-tools"

                                 "ncurses" ;for the search path
                                 
                                 ;; Wayland
                                 "pipewire"
                                 "xdg-desktop-portal"

                                 ;; JACK
                                 "alsa-utils"
                                 "alsa-plugins"
                                 "alsa-plugins:jack"
                                 "jack"
                                 "qjackctl"
                                 "gst-plugins-good"
                                 ;; Standard FreeDesktop directory paths
                                 "xdg-user-dirs"
                                 "xdg-utils"
                                 ;; Nix package manager
                                 "nix"
                                 ;; HTTPS
                                 "nss-certs"
                                 ;; User mounts
                                 "gvfs"
                                 ;; Fonts
                                 "font-awesome" ;For emacs-all-the-icons?
                                 "font-dejavu"
                                 "font-gnu-freefont"
                                 "font-ghostscript"

                                 ;; Filesystems
                                 "ntfs-3g"

                                 ;; External Monitors
                                 "ddcutil"
                                 "ddcui"

                                 ;;OpenGPG
                                 "seahorse"
                                 "gnupg"
                                 "pinentry"
                                 "pinentry-tty"
                                 "pinentry-gtk2"
                                 "pinentry-gnome3"

                                 ;; Misc
                                 "ristretto"
                                 "lsof"
                                 "jq"
                                 "ncdu"
                                 "tree"

                                 ;; Network administration
                                 "bind"
                                 "bind:utils"
                                 "tcpdump"

                                 ;; Dictionaries
                                 "hunspell-dict-it-it"
                                 "hunspell-dict-en"

                                 ;; Gnome Extensions
                                 "gnome-user-share"
                                 "gnome-shell-extension-gsconnect"
                                 "rygel"

                                 ;; Btrfs
                                 "btrfs-progs"
                                 "compsize"

                                 ;; Hardware
                                 "solaar"

                                 "virt-manager"))
                      %base-packages))

    (services
     %small-guix-desktop-services)

    ;; Allow resolution of '.local' host names with mDNS.
    (name-service-switch %mdns-host-lookup-nss)))
