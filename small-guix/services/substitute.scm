(define-module (small-guix services substitute)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (small-guix self)
  #:export (%small-guix-substitute-urls %small-guix-authorized-keys))

(define %small-guix-substitute-urls
  (append %default-substitute-urls
          (list "https://substitutes.nonguix.org"
                "https://substitutes.guix.psychnotebook.org")))

(define %small-guix-authorized-keys
  (append %default-authorized-guix-keys
          (list (file-append %small-guix-config-dir
                             "/keys/guix/substitutes.nonguix.org.pub")
                (file-append %small-guix-config-dir
                 "/keys/guix/substitutes.guix.psychnotebook.org.pub"))))
