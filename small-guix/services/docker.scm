(define-module (small-guix services docker)
  #:use-module (gnu)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages docker)
  #:use-module (guix gexp)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 format)
  #:export (oci-container-configuration
            oci-container-service-type
            oci-container-shepherd-service))

(define (sanitize-pair pair delimiter)
  (cond ((file-like? (car pair))
         (file-append (car pair) delimiter (cdr pair)))
        ((gexp? (car pair))
         (file-append (car pair) delimiter (cdr pair)))
        ((string? (car pair))
         (string-append (car pair) delimiter (cdr pair)))
        (else
         (error
          (format #f "pair members must only contain gexps, file-like objects and strings but ~a was found" (car pair))))))

(define (sanitize-mixed-list name value delimiter)
  (map
   (lambda (el)
    (cond ((string? el) el)
          ((pair? el) (sanitize-pair el delimiter))
          (else (error (format #f "~a members must be either a string or a pair but ~a was found!" name el)))))
   value))

(define (sanitize-environment value)
  ;; Expected spec format:
  ;; '(("HOME" . "/home/nobody") "JAVA_HOME=/java")
  (sanitize-mixed-list "environment" value "="))

(define (sanitize-ports value)
  ;; Expected spec format:
  ;; '(("8088" . "80") "2022:22")
  (sanitize-mixed-list "ports" value ":"))

(define (sanitize-volumes value)
  ;; Expected spec format:
  ;; '(("/mnt/dir" . "/dir") "/run/current-system/profile:/java")
  (sanitize-mixed-list "volumes" value ":"))

(define-configuration oci-container-configuration
  (command
   (list-of-strings '())
   "Overwrite the default CMD of the image.")
  (entrypoint
   (string "")
   "Overwrite the default ENTRYPOINT of the image.")
  (environment
   (list '())
   "Set environment variables."
   (sanitizer sanitize-environment))
  (image
   (string)
   "The image used to build the container.")
  (name
   (string "")
   "Set a name for the spawned container.")
  (network
   (string "")
   "Set a Docker network for the spawned container.")
  (ports
   (list '())
   "Set the port or port ranges to expose from the spawned container."
   (sanitizer sanitize-ports))
  (volumes
   (list '())
   "Set volume mappings for the spawned container."
   (sanitizer sanitize-volumes))
  (no-serialization))

(define oci-container-configuration->options
  (lambda (config)
    (let ((entrypoint
           (oci-container-configuration-entrypoint config))
          (network
           (oci-container-configuration-network config)))
      (apply append
             (filter (compose not unspecified?)
                     `(,(when (not (string-null? entrypoint))
                         (list "--entrypoint" entrypoint))
                       ,(append-map
                         (lambda (spec)
                           (list "--env" spec))
                         (oci-container-configuration-environment config))
                       ,(when (not (string-null? network))
                          (list "--network" network))
                       ,(append-map
                         (lambda (spec)
                           (list "-p" spec))
                         (oci-container-configuration-ports config))
                       ,(append-map
                         (lambda (spec)
                           (list "-v" spec))
                         (oci-container-configuration-volumes config))))))))

(define (guess-name name image)
  (if (not (string-null? name))
      name
      (string-append "docker-"
                     (basename (car (string-split image #\:))))))

(define (oci-container-shepherd-service config)
  (let* ((docker-command (file-append docker-cli "/bin/docker"))
         (config-name (oci-container-configuration-name config))
         (image (oci-container-configuration-image config))
         (name (guess-name config-name image)))
    (shepherd-service (provision `(,(string->symbol name)))
                      (requirement '(dockerd))
                      (respawn? #f)
                      (documentation
                       (string-append "Docker backed Shepherd service for image: "
                                      image))
                      (start
                       #~(make-forkexec-constructor
                          ;; docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
                          (list #$docker-command
                                "run"
                                "--rm"
                                "--name" #$name
                                #$@(oci-container-configuration->options config)
                                #$(oci-container-configuration-image config)
                                #$@(oci-container-configuration-command config))
                          #:user "root"
                          #:group "root"))
                      (stop
                       #~(lambda _
                           (invoke #$docker-command "stop" #$name))))))

(define (configs->shepherd-services configs)
  (map oci-container-shepherd-service configs))

(define oci-container-service-type
  (service-type (name 'oci-containers)
                (extensions (list ;; (service-extension account-service-type
                                  ;;                    (const %drone-ci-accounts))
                             (service-extension profile-service-type
                                                (lambda _ (list docker-cli)))
                             (service-extension
                              shepherd-root-service-type
                              configs->shepherd-services)))
                (default-value '())
                (extend append)
                (compose concatenate)
                (description
                 "This service provides a way to run Docker containers as Shepherd services.")))
