(define-module (small-guix services unattended-upgrades)
  #:use-module (guix gexp)
  #:use-module (gnu)
  #:use-module (gnu services admin)
  #:use-module (ice-9 format)
  #:export (small-guix-unattended-upgrades-service))

(define %unattended-upgrades-channels
  #~(cons* (channel
            (name 'deployments)
            (url "https://gitlab.com/orang3/guix-deployments.git")
            (branch "main"))
           (channel
            (name 'small-guix)
            (url "https://gitlab.com/orang3/small-guix")
            ;; Enable signature verification:
            (introduction
             (make-channel-introduction
              "940e21366a8c986d1e10a851c7ce62223b6891ef"
              (openpgp-fingerprint
               "D088 4467 87F7 CBB2 AE08  BE6D D075 F59A 4805 49C3"))))
           (channel
            (name 'nonguix)
            (url "https://gitlab.com/nonguix/nonguix")
            ;; Enable signature verification:
            (introduction
             (make-channel-introduction
              "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
              (openpgp-fingerprint
               "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
           %default-channels))

(define* (small-guix-unattended-upgrades-service host-name #:key (channels %unattended-upgrades-channels) (hours 23) (minutes 10))
  (let ((host-name-symbol
         (string->symbol host-name)))
    (service unattended-upgrade-service-type
         (unattended-upgrade-configuration
          (schedule (format #f "~a ~a * * *" minutes hours))
          (system-expiration
           (* 7 24 3600))
          (channels channels)
          (operating-system-expression
           #~(@ (#$host-name-symbol system config)
                #$(symbol-append host-name-symbol '-system)))))))
