;; This has been taken from https://gitlab.com/mbakke/guix-sway-example/-/blob/master/config/sway.scm
(define-module (small-guix services sway)
  #:use-module (guix gexp)
  #:use-module (gnu services)
  #:use-module (gnu system pam)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages wm)
  #:export (%sway-variables %sway-environment-service %sway-login-shell))

(define %sway-variables

  '(("CLUTTER_BACKEND" . "wayland") ;GTK
    ("QT_QPA_PLATFORM" . "wayland") ;Qt
    ("MOZ_ENABLE_WAYLAND" . "1") ;IceCat, et.al.
    ;; These are normally provided by login managers(?).
    ("XDG_SESSION_TYPE" . "wayland")
    ("XDG_SESSION_DESKTOP" . "sway")
    ("XDG_CURRENT_DESKTOP" . "sway")

    ;; https://github.com/swaywm/sway/issues/595
    ("_JAVA_AWT_WM_NONREPARENTING" . "1")))

(define %sway-environment-service
  (simple-service 'sway-environment session-environment-service-type
                  %sway-variables))

(define %sway-login-shell
  (program-file "login-shell"
                #~(let ((bash #$(file-append bash "/bin/bash"))
                        (dbus-run-session #$(file-append dbus
                                             "/bin/dbus-run-session"))
                        (sway #$(file-append sway "/bin/sway"))
                        (tty (readlink "/proc/self/fd/0"))
                        (self (current-load-port))
                        (args (cdr (program-arguments))))
                    (fcntl self F_SETFD
                           (logior FD_CLOEXEC
                                   (fcntl self F_GETFD)))
                    (if (and (string-prefix? "/dev/tty" tty)
                             (string-suffix? "1" tty)
                             (not (getenv "DISPLAY")))
                        (begin
                          (execl bash bash "--login" "-c"
                                 (string-append "exec " dbus-run-session
                                                " sway-starter")))
                        (begin
                          (apply execl bash bash args))))))
