(define-module (small-guix services base)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services base)
  #:use-module (small-guix services substitute)
  #:export (%small-guix-base-services))

(define %small-guix-base-services
  (modify-services %base-services
    (guix-service-type config =>
                       (guix-configuration (inherit config)
                                           (substitute-urls
                                            %small-guix-substitute-urls)
                                           (authorized-keys
                                            %small-guix-authorized-keys)))))
