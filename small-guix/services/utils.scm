(define-module (small-guix services utils)
  #:use-module (gnu)
  #:use-module (guix gexp)
  #:export (logger-wrapper))

(use-package-modules admin)
 ;for inetutils

;; Thanks to
;; https://lists.gnu.org/archive/html/guix-devel/2019-06/msg00230.html

(define* (logger-wrapper name exec . args)
  "Return a derivation that builds a script to start a process with
standard output and error redirected to syslog via logger."
  (define exp
    #~(begin
        (use-modules (ice-9 popen))
        (let* ((pid (number->string (getpid)))
               (logger #$(file-append inetutils "/bin/logger"))
               (args (list "-t"
                           #$name
                           (string-append "--id=" pid)))
               (pipe (apply open-pipe* OPEN_WRITE logger args)))
          (dup pipe 1)
          (dup pipe 2)
          (execl #$exec
                 #$exec
                 #$@args))))
  (program-file (string-append name "-logger") exp))
