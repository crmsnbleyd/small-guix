;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix services mcron)
  #:use-module (gnu packages base)  ;for findutils
  #:use-module (guix gexp)
  #:use-module (small-guix packages scripts)
  #:use-module (ice-9 format)
  #:export (updatedb-job))

;; Thanks to https://guix.gnu.org/en/manual/devel/en/guix.html#Scheduled-Job-Execution
(define-public updatedb-job
  ;; Run 'updatedb' at 23:15 every day.
  #~(job "15 23 * * *"
         (lambda ()
           (system* (string-append #$findutils "/bin/updatedb")
                    "--prunepaths=/tmp /var/tmp /gnu/store /nix/store"))
         "updatedb"))
