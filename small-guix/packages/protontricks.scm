(define-module (small-guix packages protontricks)
  #:use-module (guix download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages python-build)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject))

(define-public python-vdf
  (package
    (name "python-vdf")
    (version "3.4")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "vdf" version))
              (sha256
               (base32
                "1bz2gn04pl6rj2mawlzlirz1ygg4rdypq0pxbyg018873vs1jm7x"))))
    (build-system pyproject-build-system)
    (home-page "https://github.com/ValvePython/vdf")
    (synopsis "Library for working with Valve's VDF text format")
    (description "Library for working with Valve's VDF text format")
    (license license:expat)))

(define-public python-protontricks
  (package
    (name "python-protontricks")
    (version "1.10.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "protontricks" version))
              (sha256
               (base32
                "129dici6qamxx7rx6bl9g7z2hz6mql8i9biws3xw87s1k7ps4wg4"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-setuptools-scm python-vdf))
    (home-page "https://github.com/Matoking/protontricks")
    (synopsis
     "A simple wrapper for running Winetricks commands for Proton-enabled games.")
    (description
     "This package provides a simple wrapper for running Winetricks commands for
Proton-enabled games.")
    (license license:gpl3+)))
