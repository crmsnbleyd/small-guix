;;; Copyright © 2020 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix packages python-xyz)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (guix utils))

(define-public python-sc3nb
  (let ((version "0.0.0")
        (revision "0")
        (commit "59f638bdc04809544f6c6894df8f6f4ba4ba8e97"))
    (package
      (name "python-sc3nb")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/thomas-hermann/sc3nb.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "121xc7wgrkpasz524fs9nh1d5pfvcg94i0dwzmvspv1vaab4n2lx"))))
      (build-system python-build-system)
      (arguments
       `(#:tests? #f)) ;There are none.
      (propagated-inputs `(("python-ipython" ,python-ipython)
                           ("python-numpy" ,python-numpy)
                           ("python-osc" ,python-osc)
                           ("python-scipy" ,python-scipy)))
      (home-page "https://github.com/thomas-hermann/sc3nb")
      (synopsis "SuperCollider3 interface for Python and Jupyter notebooks")
      (description
       "@code{sc3nb} is a Python package that offers a SuperCollider3
interface, with special support to be used within Jupyter notebooks.  It establishes
shortcuts and familiar functions used by @code{sclang} to make it easier to interactively
try sound synthesis and sonification development.")
      (license license:expat))))

(define-public python-pya
  (package
    (name "python-pya")
    (version "0.4.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "pya" version))
              (sha256
               (base32
                "1ixxiinq3nnk36qln3dc6f4xbnplwcm0hsfrvlrig7whjgkgc3i0"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f)) ;They depend on sanic.
    (propagated-inputs `(("python-matplotlib" ,python-matplotlib)
                         ("python-numpy" ,python-numpy)
                         ("python-pyaudio" ,python-pyaudio)
                         ("python-scipy" ,python-scipy)
                         ("python-sanic" ,python-sanic)))
    (home-page "https://github.com/interactive-sonification/pya")
    (synopsis "Python audio coding classes - for dsp and sonification")
    (description "Python audio coding classes - for dsp and sonification")
    (license license:expat)))

(define-public python-pyfim
  (package
    (name "python-pyfim")
    (version "6.28")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "pyfim" version))
              (sha256
               (base32
                "11ajsx9dswsczxh1xq9k2m5spyf2y8sm8krl5qjc45w3rbcrklbn"))))
    (build-system python-build-system)
    (home-page "http://www.borgelt.net/pyfim.html")
    (synopsis
     "Frequent Item Set Mining and Association Rule Induction for Python")
    (description
     "PyFIM is an extension module that makes several frequent item set mining
implementations available as functions in Python 2.7.x & 3.8.x.  Currently
@url{https://borgelt.net/apriori.html, apriori}, @url{https://borgelt.net/eclat.html, eclat},
@url{https://borgelt.net/fpgrowth.html, fpgrowth}, @url{https://borgelt.net/sam.html, sam},
@url{https://borgelt.net/relim.html, relim}, @url{https://borgelt.net/carpenter.html, carpenter},
@url{https://borgelt.net/ista.html, ista}, @url{https://borgelt.net/accretion.html, accretion} and
@url{https://borgelt.net/apriori.html, apriacc} are available as functions, although the interfaces
do not offer all of the options of the command line program.  (Note that @code{lcm} is available as
an algorithm mode of @code{eclat}).  There is also a \"generic\" function @code{fim}, which is essentially
the same function as @code{fpgrowth}, only with a simplified interface (fewer options).

Finally, there is a function @code{arules} for generating association rules (simplified interface compared
to @code{apriori}, @code{eclat} and @code{fpgrowth}, which can also be used to generate association rules.")
    (license license:expat)))
