;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (small-guix packages php-xdebug)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages php)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public xdebug
  (package
    (name "xdebug")
    (version "2.9.5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/xdebug/xdebug.git")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1f37kdb3f8g58sl5hlabkq9nn0pa9amh1sr7qrjgbwiymv036msz"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags '("--enable-xdebug")
       #:phases (modify-phases %standard-phases
                  (add-before 'configure 'phpize
                    (lambda _
                      (invoke "phpize")))
                  (replace 'check
                    (lambda _
                      ;; Some tests require network access.
                      (setenv "TEST_PHP_ARGS" "--offline")
                      (setenv "SKIP_DBGP_TESTS" "1")
                      (for-each delete-file
                                '("tests/debugger/bug00964-001.phpt"
                                  "tests/debugger/bug00964-002.phpt"
                                  "tests/debugger/bug01708.phpt"))
                      (invoke "make" "test")))
                  (replace 'install
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let ((lib (string-append (assoc-ref outputs "out")
                                                "/lib")))
                        (mkdir-p lib)
                        (install-file "modules/xdebug.so" lib) #t))))))
    (native-inputs `(("autoconf" ,autoconf)
                     ("php" ,php)))
    (home-page "https://xdebug.org")
    (synopsis "Extension for PHP to assist with debugging and development")
    (description
     "Xdebug is an extension for PHP to assist with debugging
and development.  It contains a single step debugger to use with IDEs, a
profiler and provides code coverage functionality for use with PHPUnit.")
    (license (license:non-copyleft "file://LICENSE"))))
 ; The Xdebug license.

(define-public php-with-xdebug
  (package
    (inherit php)
    (name "php-with-xdebug")
    (arguments
     (substitute-keyword-arguments (package-arguments php)
       ((#:phases phases)
        `(modify-phases ,phases
           (add-after 'install 'install-xdebug
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let ((xdebug.so (string-append (assoc-ref inputs "xdebug")
                                               "/lib/xdebug.so"))
                     (out (assoc-ref outputs "out")))
                 (invoke "bash" "-c"
                         (string-append out "/bin/php"
                          " -i | grep 'PHP API => ' | sed -E 's/.+=> //' > version"))
                 (use-modules (ice-9 textual-ports))
                 (call-with-input-file "version"
                   (lambda (port)
                     (let ((ext (string-append out
                                 "/lib/php/extensions/no-debug-non-zts-"
                                 (get-line port))))
                       (mkdir-p ext)
                       (install-file xdebug.so ext)
                       (let ((output-port (open-file (string-append out
                                                      "/lib/php.ini") "a")))
                         (display (string-append "zend_extension = " ext
                                                 "/xdebug.so\nxdebug.remote_enable=1\n")
                                  output-port)
                         (close output-port))))))))))))
    (inputs `(("xdebug" ,xdebug)
              ,@(package-inputs php)))))
