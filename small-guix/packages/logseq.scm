;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2022, 2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix packages logseq)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages nss)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages photo)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages video)
  #:use-module (gnu packages wget)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (nonguix build-system binary)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (ice-9 string-fun))

(define-public logseq
  (let ((patchelf-inputs
         (list "alsa-lib" "at-spi2-atk" "at-spi2-core" "atk" "cairo" "cups"
                "dbus" "eudev" "expat" "freetype" "fontconfig-minimal" "gcc" "gdk-pixbuf"
                "glib" "gtk+" "harfbuzz" "libdrm" "libnotify" "libsecret" "libx11"
                 "libxcb" "libexif" "libxcomposite" "libxcursor" "libxdamage"
                 "libxext" "libxfixes" "libxi" "libxkbcommon" "libxkbfile" "libxrandr"
                 "libxrender" "libxtst" "libnotify" "mesa" "nspr" "pango" "pipewire"
                 "sqlcipher" "xdg-utils" "zlib")))
   (package
    (name "logseq")
    (version "0.9.2")
    (source
        (origin
          (method url-fetch)
          (uri (string-append "https://github.com/logseq/logseq/releases/download/"version"/Logseq-linux-x64-" version ".zip"))
          (file-name (string-append "logseq.zip"))
          (sha256
           (base32
            "1p1r2ycramrk164y5348qvvlfhhpb0bxwd9lgwkmvbh1xp14xb45"))))
    (build-system binary-build-system)
    (arguments
     (list
       ;; almost 300MB
       #:substitutable? #f
       #:patchelf-plan
        #~(let ((patchelf-inputs (list #$@patchelf-inputs)))
            (map (lambda (file)
                     (cons file (list patchelf-inputs)))
               '("Logseq"
                 "chrome-sandbox"
                 "chrome_crashpad_handler"
                 "libEGL.so"
                 "libffmpeg.so"
                 "libGLESv2.so"
                 "libvk_swiftshader.so"
                 "libvulkan.so.1"
                 "resources/app/node_modules/better-sqlite3/build/Release/better_sqlite3.node"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git-daemon"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git-http-backend"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git-http-fetch"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git-http-push"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git-imap-send"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git-remote-http"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git-shell"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git-sh-i18n--envsubst"
                 "resources/app/node_modules/dugite/git/bin/git"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git"
                 "resources/app/node_modules/@logseq/rsapi-linux-x64-gnu/rsapi.linux-x64-gnu.node"
                 "resources/app/node_modules/dugite/git/libexec/git-core/git-daemon")))
       #:install-plan
        #~'(("." "/share"))
       #:phases
        #~(modify-phases %standard-phases
            (delete 'validate-runpath)
            (replace 'unpack
              (lambda* (#:key inputs #:allow-other-keys)
                 (invoke "unzip" #$source)
                 (chdir "Logseq-linux-x64")))
            (add-after 'install 'install-wrapper
             (lambda _
               (let* ((bin (string-append #$output "/bin"))
                      (exe (string-append bin "/Logseq"))
                      (share (string-append #$output "/share"))
                      (target (string-append share "/Logseq"))
                      (patchelf-inputs-packages (list #$@(map (lambda (i) (this-package-input i)) patchelf-inputs)))
                      (ld-library-libs (map (lambda (input)
                                              (string-append input "/lib"))
                                            patchelf-inputs-packages)))
                 (mkdir-p bin)
                 (symlink target exe)
                 (wrap-program exe
                   `("FONTCONFIG_PATH" ":" prefix
                      (,(string-join
                         (list
                          (string-append #$(this-package-input "fontconfig-minimal") "/etc/fonts")
                          #$output)
                         ":")))
                  `("LD_LIBRARY_PATH" ":" prefix
                     (,(string-join
                        (append
                          (list
                           share
                           #$output
                           (string-append #$(this-package-input "nss") "/lib/nss"))
                          ld-library-libs)
                        ":"))))))))))
    (native-inputs (list unzip))
    (inputs
      (list alsa-lib
            at-spi2-atk
            at-spi2-core
            atk
            bzip2
            cairo
            curl
            cups
            dbus
            eudev
            expat
            flac
            fontconfig
            freetype
            font-liberation
            `(,gcc "lib")
            gdk-pixbuf
            glib
            gtk+
            harfbuzz
            libdrm
            libexif
            libglvnd
            libnotify
            libpng
            librsvg
            libsecret
            libva
            libx11
            libxcb
            libxcomposite
            libxcursor
            libxdamage
            libxext
            libxfixes
            libxi
            libxkbcommon
            libxkbfile
            libxrandr
            libxscrnsaver
            libxshmfence
            libxrender
            libxtst
            mesa
            nspr
            nss
            opus
            pango
            pciutils
            pipewire
            snappy
            sqlcipher
            util-linux
            xdg-utils
            wget
            zlib))
    (synopsis "Note taking app")
    (supported-systems '("x86_64-linux"))
    (description "Logseq is a cross-platform note taking app.")
    (home-page "https://logseq.com/")
    (license license:expat))))
