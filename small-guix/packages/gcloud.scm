;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix packages gcloud)
  #:use-module (ice-9 match)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages web)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system python)
  #:use-module (small-guix utils))

(define-public python-crcmod
  (package
    (name "python-crcmod")
    (version "1.7")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "crcmod" version))
              (sha256
               (base32
                "07k0hgr42vw2j92cln3klxka81f33knd7459cn3d8aszvfh52w6w"))))
    (build-system python-build-system)
    (home-page "http://crcmod.sourceforge.net/")
    (synopsis "CRC Generator")
    (description "CRC Generator")
    (license license:expat)))

(define-public google-cloud-cli
  (package
    (name "google-cloud-cli")
    (version "447.0.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/"
                    "google-cloud-cli-" version "-linux-x86_64.tar.gz"))
              (patches
               ;; https://github.com/NixOS/nixpkgs/tree/master/pkgs/tools/admin/google-cloud-sdk
               (parameterize
                   ((%patch-path
                     (map (lambda (directory)
                            (string-append directory "/small-guix/packages/patches"))
                          %load-path)))
                (search-patches
                 ;; For kubectl configs, don't store the absolute path of the
                 ;; `gcloud` binary as it can be garbage-collected
                 "gcloud-path.patch"
                 ;; Disable checking for updates for the package
                 "gsutil-disable-updates.patch")))
              (sha256
               (base32
                "0f1gx45fg5d3vbdhbigr3qsd778aj341594i61aqpk14kmszwllb"))))
    (build-system copy-build-system)
    (arguments
     (list #:install-plan #~'(("google-cloud-sdk/" "opt/google-cloud-sdk/"))
           #:imported-modules `((guix build python-build-system)
                                ,@%copy-build-system-modules)
           #:phases #~(modify-phases %standard-phases
                        (replace 'unpack
                          (lambda _
                            (invoke "tar" "-xvf" #$source)))
                        (add-after 'unpack 'cleanup
                          (lambda _
                            (let ((here (string-append (getcwd) "/google-cloud-sdk")))
                              ;; This directory bundles an entire python interpreter source code.
                              (delete-file-recursively (string-append here "/platform/bundledpythonunix"))
                              ;; This directory contains compiled mac binaries.
                              (for-each (lambda (dir)
                                          (delete-file-recursively (string-append here "/" dir)))
                                        '("platform/gsutil/third_party/crcmod"
                                          "platform/gsutil/third_party/crcmod_osx"))
                              ;; remove tests and test data
                              (invoke "find" here "-name" "tests" "-type" "d" "-exec" "rm" "-rf" "{}" "+")
                              (delete-file (string-append here "/platform/gsutil/gslib/commands/test.py"))
                              (for-each (lambda (json)
                                          (invoke "sh" "-x" "-c" (string-append "cat " json " | jq -c . > " json ".min"))
                                          (invoke "mv" (string-append json ".min") json))
                                        (find-files here "\\.json$")))))
                        (add-after 'cleanup 'disable-updates
                          (lambda _
                            (let ((here (string-append (getcwd) "/google-cloud-sdk")))
                              ;; disable component updater and update check
                              (substitute* (string-append here "/lib/googlecloudsdk/core/config.json")
                                (("\"disable_updater\": false")
                                 "\"disable_updater\": true"))
                              (let ((output-port (open-file (string-append here "/properties") "a")))
                                (display "
    [component_manager]
    disable_update_check = true" output-port)))))
                        (add-after 'install 'symlink-binaries
                          (lambda _
                            (symlink (string-append #$output "/opt/google-cloud-sdk/bin")
                                     (string-append #$output "/bin"))))
                        (add-after 'add-install-to-pythonpath 'wrap-for-python
                          (@@ (guix build python-build-system) wrap))
                        (add-after 'symlink-binaries 'install-wrapper
                           (lambda* (#:key inputs outputs #:allow-other-keys)
                             (use-modules (srfi srfi-1)
                                          (guix build python-build-system))
                             (add-installed-pythonpath inputs outputs)
                             (let* ((bin (string-append #$output "/bin"))
                                    (inputs
                                     (list
                                      #$@(map cadr
                                              (package-inputs this-package))))
                                    (obtain-dirs-for-input
                                     (lambda (dirs input)
                                       (filter-map
                                        (lambda (sub-directory)
                                         (let ((directory
                                                (string-append
                                                 input "/" sub-directory)))
                                           (and
                                            (directory-exists? directory)
                                            directory)))
                                        dirs)))
                                    (obtain-dirs-from-inputs
                                     (lambda (dirs)
                                      (reduce append '()
                                       (map
                                        (lambda (input)
                                          (obtain-dirs-for-input dirs input))
                                        inputs))))
                                    (bin-directories
                                     (obtain-dirs-from-inputs
                                      '("bin" "sbin" "libexec"))))
                               (for-each (lambda (exe)
                                           (display "Wrapping ")
                                           (display exe)
                                           (newline)
                                           (wrap-program exe
                                             `("CLOUDSDK_PYTHON" = (,(which "python3")))
                                             `("CLOUDSDK_CORE_DISABLE_USAGE_REPORTING" = ("Y"))
                                             `("GUIX_PYTHONPATH" ":" prefix (,(getenv "GUIX_PYTHONPATH")))
                                             `("PATH" ":" prefix
                                               (,(string-join bin-directories ":")))))
                                         (map (lambda (file)
                                                (string-append #$output "/bin/" file))
                                              '("gcloud"
                                                "bq"
                                                "gsutil"
                                                "git-credential-gcloud.sh"
                                                "docker-credential-gcloud")))))))))
    (native-inputs
     (list findutils
           jq))
    (inputs
     (list python
           openssl))
    (propagated-inputs
     (list python-cffi
           python-cryptography
           python-crcmod))
    (home-page "https://cloud.google.com/cli")
    (supported-systems '("x86_64-linux"))
    (synopsis "Google Cloud Command Line Interface")
    (description
     "Create and manage Google Cloud resources and services directly on the
command line or via scripts using the Google Cloud CLI.")
    (license license:expat)))

(define-public google-cloud-sdk-gke-gcloud-auth-plugin
  (package
    (name "google-cloud-sdk-gke-gcloud-auth-plugin")
    (version "420.0.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://storage.googleapis.com/cloud-sdk-release/"
                    "for_packagers/linux/"
                    "google-cloud-cli-gke-gcloud-auth-plugin_" version
                    ".orig_amd64.tar.gz"))
              (sha256
               (base32
                "04k34s7pjyx4ndynh27g6vq72apizipyksp3dlsjl2654vyxj6ia"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       `(("google-cloud-sdk/bin/gke-gcloud-auth-plugin" "bin/")
         ("google-cloud-sdk/.install" "opt/google-cloud-sdk"))
       #:phases (modify-phases %standard-phases
                  (replace 'unpack
                    (lambda* (#:key inputs #:allow-other-keys)
                      (invoke "tar" "-xvf" (assoc-ref inputs "source"))))
                  (add-before 'install 'chmod
                    (lambda _
                      (chmod "google-cloud-sdk/bin/gke-gcloud-auth-plugin" #o555))))))
    (home-page "https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke")
    (supported-systems '("x86_64-linux"))
    (synopsis "kubectl authentication plugin for GKE")
    (description
     "A google-cloud-sdk component that provides a kubectl authentication plugin
for GKE.")
    (license #f)))
