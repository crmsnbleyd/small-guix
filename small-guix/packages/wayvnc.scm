;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix packages wayvnc)
  #:use-module (ice-9 match)
  #:use-module (guix build-system meson)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages xdisorg))

(define-public aml
  (package
    (name "aml")
    (version "0.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/any1/aml")
                    (commit (string-append "v" version))))
              (sha256
               (base32
                "1m911n3rd41ch4yk3k9k1lz29xp3h54k6jx122abq5kmngy9znqw"))
              (file-name (git-file-name name version))))
    (build-system meson-build-system)
    (native-inputs (list pkg-config))
    (home-page "https://github.com/any1/aml")
    (synopsis "Another Main Loop")
    (description "Andri's Main Loop. Features include:

@itemize

@item File descriptor event handlers
@item Timers
@item Tickers
@item Signal handlers
@item Idle dispatch callbacks
@item Thread pool
@item Interoperability with other event loops

@end itemize
")
    (license license:isc)))

(define-public neatvnc
  (package
    (name "neatvnc")
    (version "0.4.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/any1/neatvnc")
                    (commit (string-append "v" version))))
              (sha256
               (base32
                "1wpq1vyjqra877vwc3n4i0c1dyhmabyn993cslf1k142ikyc0a8w"))
              (file-name (git-file-name name version))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags '("-Djpeg=enabled" "-Dtls=enabled")))
    (native-inputs (list libdrm pkg-config))
    (inputs (list aml gnutls libjpeg-turbo pixman zlib))
    (home-page "https://github.com/any1/neatvnc")
    (synopsis "VNC server library")
    (description
     "neatvnc is a liberally licensed VNC server library that's intended to be
fast.")
    (license license:isc)))

(define-public wayvnc
  (package
    (name "wayvnc")
    (version "0.4.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/any1/wayvnc")
                    (commit (string-append "v" version))))
              (sha256
               (base32
                "0cws9jfnmxqycmlyllvvqzw4jsbrwwk10v9gy8wifv3c61rwgdkk"))
              (file-name (git-file-name name version))))
    (build-system meson-build-system)
    (arguments
     `(#:configure-flags '("-Dman-pages=enabled" "-Dpam=enabled")))
    (native-inputs (list cmake
                         gnutls
                         libjpeg-turbo
                         pkg-config
                         scdoc
                         zlib))
    (inputs (list aml
                  libdrm
                  libxkbcommon
                  linux-pam
                  neatvnc
                  pixman
                  wayland))
    (home-page "https://github.com/any1/wayvnc")
    (synopsis "VNC server for wlroots based Wayland compositors")
    (description
     "wayvnc is a VNC server for wlroots-based Wayland compositors.
It attaches to a running Wayland session, creates virtual input devices, and
exposes a single display via the RFB protocol.  The Wayland session may be a
headless one, so it is also possible to run wayvnc without a physical display
attached.")
    (license license:isc)))
