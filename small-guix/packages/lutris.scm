;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This file is part of Small Guix.
;;;
;;; Small Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Small Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Small Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (small-guix packages lutris)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages file)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages messaging)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages wine)
  #:use-module (nongnu packages wine)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python))

;; Since 7.66.0 libcurl doesn't allow HTTP-0.9
;; anymore. For some reason some lutris scripts
;; require to use it.
(define curl-7.65.3
  (package
    (inherit curl)
    (version "7.65.3")
    (source (origin
             (method url-fetch)
             (uri (string-append "https://curl.se/download/curl-"
                                 version ".tar.xz"))
             (sha256
              (base32
               "1sjz4fq7jg96mpmpqq82nd61njna6jp3c4m9yrbx2j1rh5a8ingj"))))))

(define-public lutris-unwrapped
  (package
    (name "lutris-unwrapped")
    (version "0.5.12")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/lutris/lutris")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0643ah7sx11fyzg8n9fga8b12lzypwn4ysx3nxlcwczznadrgj5f"))))
    (build-system meson-build-system)
    (arguments
     (list #:glib-or-gtk? #t
           #:imported-modules `((guix build python-build-system)
                                ,@%meson-build-system-modules)
           #:phases #~(modify-phases %standard-phases
                        (add-after 'unpack 'patch-libraries
                          (lambda _
                            (substitute* "lutris/util/linux.py"
                              (("libvulkan\\.so\\.1")
                               #$(file-append (this-package-input "vulkan-loader")
                                              "/lib/libvulkan.so.1"))
                              (("libGL\\.so\\.1")
                               #$(file-append (this-package-input "mesa")
                                              "/lib/libGL.so.1"))
                              (("libsqlite3\\.so\\.0")
                               #$(file-append (this-package-input "sqlite")
                                              "/lib/libsqlite3.so.0"))
                              (("libgnutls\\.so\\.30")
                               #$(file-append (this-package-input "gnutls")
                                              "/lib/libgnutls.so.30")))
                            (substitute* "lutris/util/graphics/vkquery.py"
                              (("libvulkan\\.so\\.1")
                               #$(file-append (this-package-input "vulkan-loader")
                                              "/lib/libvulkan.so.1")))
                            (substitute* "lutris/util/magic.py"
                              (("CDLL\\(dll")
                               (string-append
                                "CDLL('"
                                #$(file-append (this-package-input "file")
                                               "/lib/libmagic.so.1.0.0")
                                "'")))))
                        (add-after 'install 'add-install-to-pythonpath
                          (@@ (guix build python-build-system)
                              add-install-to-pythonpath))
                        (add-after 'add-install-to-pythonpath 'wrap-for-python
                          (@@ (guix build python-build-system) wrap))
                        (add-after 'wrap-for-python 'wrap-with-binaries
                          (lambda _
                            (use-modules (srfi srfi-1))
                            (let* ((bin (string-append #$output "/bin"))
                                   (exe (string-append bin "/lutris"))
                                   (inputs
                                    (list
                                     #$@(map cadr
                                             (package-inputs this-package))))
                                   (obtain-dirs-for-input
                                    (lambda (dirs input)
                                      (filter-map
                                       (lambda (sub-directory)
                                         (let ((directory
                                                (string-append
                                                 input "/" sub-directory)))
                                           (and
                                            (directory-exists? directory)
                                            directory)))
                                       dirs)))
                                   (obtain-dirs-from-inputs
                                    (lambda (dirs)
                                      (reduce append '()
                                       (map
                                        (lambda (input)
                                          (obtain-dirs-for-input dirs input))
                                        inputs))))
                                   (lib-directories
                                    (obtain-dirs-from-inputs '("lib")))
                                   (bin-directories
                                    (obtain-dirs-from-inputs
                                     '("bin" "sbin" "libexec")))
                                   (girepo-directories
                                    (obtain-dirs-from-inputs
                                     '("lib/girepository-1.0"))))

                              (wrap-program exe
                                `("PATH" ":" prefix
                                  (,(string-join bin-directories ":")))
                                `("LD_LIBRARY_PATH" ":" prefix
                                  (,(string-join lib-directories ":")))
                                `("GI_TYPELIB_PATH" ":" prefix
                                  (,(string-join girepo-directories ":")))
                                `("GDK_PIXBUF_MODULE_FILE" =
                                  (,(getenv "GDK_PIXBUF_MODULE_FILE")))
                                `("GIO_EXTRA_MODULES" =
                                  (,(getenv "GIO_EXTRA_MODULES"))))))))))
    (native-inputs (list desktop-file-utils
                         `(,gtk+ "bin")
                         gettext-minimal
                         gobject-introspection
                         glib
                         wine
                         xorg-server))
    (inputs (list adwaita-icon-theme
                  atk
                  cabextract
                  curl-7.65.3
                  file
                  fluidsynth
                  flatpak-xdg-utils
                  dbus
                  gdk-pixbuf
                  glib-networking
                  gnutls
                  gtk+
                  gsettings-desktop-schemas
                  gst-libav
                  gst-plugins-bad
                  gst-plugins-base
                  gst-plugins-good
                  gst-plugins-ugly
                  gstreamer
                  harfbuzz
                  hicolor-icon-theme
                  libnotify
                  libpng
                  librsvg
                  libsoup
                  mesa
                  mesa-utils
                  pango
                  p7zip
                  pciutils
                  psmisc
                  pulseaudio
                  setxkbmap
                  sqlite
                  unzip
                  util-linux
                  vulkan-loader
                  vulkan-tools
                  webkitgtk
                  wine
                  winetricks
                  xkbcomp
                  xgamma
                  xrandr))
    (propagated-inputs (list python-dbus
                             python-certifi
                             python-distro
                             python-evdev
                             python-pygobject
                             python-lxml
                             python-magic
                             python-pillow
                             python-requests
                             python-pypresence
                             python-pyyaml
                             python-wrapper
                             xterm))
    (home-page "https://lutris.net/")
    (synopsis "Install video games from most gaming platforms")
    (description
     "Lutris helps you install and play video games from all eras
and from most gaming systems.  By leveraging and combining existing emulators,
engine re-implementations and compatibility layers, it gives you a central
interface to launch all your games.

The client can connect with existing services like Humble Bundle, GOG and Steam
to make your game libraries easily available.  Game downloads and installations
are automated and can be modified through user made scripts.")
    (license license:gpl3+)))
