;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2022, 2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix packages papeer)
  #:use-module (guix build-system go)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix transformations)
  #:use-module (gnu)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages syncthing)
  #:use-module (gnu system)
  #:export (small-guix-unattended-upgrades))

(define cascadia-1.3
  (package
    (inherit go-github-com-andybalholm-cascadia)
    (name (package-name go-github-com-andybalholm-cascadia))
    (version "1.3.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/andybalholm/cascadia")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0zgc9fjkn7d66cnmgnmalr9lrq4ii1spap95pf2x1hln4pflib5s"))))))

(define-public patch-cascadia
  (package-input-rewriting/spec `((,(package-name go-github-com-andybalholm-cascadia) . ,(const cascadia-1.3)))))

(define-public go-github-com-sebdah-goldie-v2
  (package
    (name "go-github-com-sebdah-goldie-v2")
    (version "2.5.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/sebdah/goldie")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "12gji9p6b6zlkisbd3ww103zwd5chlwkb6h5dppfrmgxim84n5n0"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/sebdah/goldie/v2"))
    (propagated-inputs (list go-github-com-stretchr-testify
                             go-github-com-sergi-go-diff
                             go-github-com-pmezard-go-difflib
                             go-github-com-pkg-errors))
    (home-page "https://github.com/sebdah/goldie")
    (synopsis "goldie - Golden test utility for Go")
    (description
     "Package goldie provides test assertions based on golden files.  It's typically
used for testing responses with larger data bodies.")
    (license license:expat)))

(define-public go-github-com-johanneskaufmann-html-to-markdown
  (package
    (name "go-github-com-johanneskaufmann-html-to-markdown")
    (version "1.3.6")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url
                     "https://github.com/JohannesKaufmann/html-to-markdown")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "085z9kilr0fw417ngdrhr04g9qj95dl7rsx5f377njyf0syxvs4z"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/JohannesKaufmann/html-to-markdown"
       #:tests? #f))
    (propagated-inputs (list go-gopkg-in-yaml-v2
                             go-golang-org-x-net
                             go-github-com-yuin-goldmark
                             go-github-com-sergi-go-diff
                             go-github-com-sebdah-goldie-v2
                             go-github-com-puerkitobio-goquery))
    (home-page "https://github.com/JohannesKaufmann/html-to-markdown")
    (synopsis "html-to-markdown")
    (description "Package md converts html to markdown.")
    (license license:expat)))

(define-public go-github-com-bmaupin-go-epub
  (package
    (name "go-github-com-bmaupin-go-epub")
    (version "1.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/bmaupin/go-epub")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0hf0fjrfgv1k81cr6irrkklcsy42mywxrrgk0z73kjlp6anspha8"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/bmaupin/go-epub"
       ;; At least one test depends on network access.
       #:tests? #f))
    (propagated-inputs `(("go-github-com-vincent-petithory-dataurl" ,go-github-com-vincent-petithory-dataurl)
                         ("go-github-com-gofrs-uuid" ,go-github-com-gofrs-uuid)
                         ("go-github-com-gabriel-vasile-mimetype" ,go-github-com-gabriel-vasile-mimetype)))
    (home-page "https://github.com/bmaupin/go-epub")
    (synopsis "Features")
    (description
     "Package epub generates valid EPUB 3.0 files with additional EPUB 2.0 table of
contents (as seen here:
@@url{https://github.com/bmaupin/epub-samples,https://github.com/bmaupin/epub-samples})
for maximum compatibility.")
    (license license:expat)))

(define-public go-github-com-go-shiori-go-readability
  (package
    (name "go-github-com-go-shiori-go-readability")
    (version "0.0.0-20220215145315-dd6828d2f09b")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-shiori/go-readability")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vnnn4957nb9xqildg1n4xjp516d6c8rp3hvckgbwdbf8c6vi58g"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-shiori/go-readability"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-spf13-pflag" ,go-github-com-spf13-pflag)
                         ("go-github-com-spf13-cobra" ,go-github-com-spf13-cobra)
                         ("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)
                         ("go-github-com-sergi-go-diff" ,go-github-com-sergi-go-diff)
                         ("go-github-com-go-shiori-dom" ,go-github-com-go-shiori-dom)))
    (home-page "https://github.com/go-shiori/go-readability")
    (synopsis "Go-Readability")
    (description
     "Package readability is a Go package that find the main readable content from a
HTML page.  It works by removing clutter like buttons, ads, background images,
script, etc.")
    (license license:expat)))

(define-public go-github-com-chzyer-logex
  (package
    (name "go-github-com-chzyer-logex")
    (version "1.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/chzyer/logex")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0c9yr3r7dl3lcs22cvmh9iknihi9568wzmdywmc2irkjdrn8bpxw"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/chzyer/logex"
       #:tests? #f))
    (home-page "https://github.com/chzyer/logex")
    (synopsis "Logex")
    (description
     "An golang log lib, supports tracing and level, wrap by standard log lib")
    (license license:expat)))

(define-public go-github-com-chzyer-readline
  (package
    (name "go-github-com-chzyer-readline")
    (version "1.5.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/chzyer/readline")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1msh9qcm7l1idpmfj4nradyprsr86yhk9ch42yxz7xsrybmrs0pb"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/chzyer/readline"
       #:tests? #f))
    (propagated-inputs `(("go-github-com-chzyer-logex" ,go-github-com-chzyer-logex)
                         ("go-golang-org-x-sys" ,go-golang-org-x-sys)))
    (home-page "https://github.com/chzyer/readline")
    (synopsis "Guide")
    (description
     "Readline is a pure go implementation for GNU-Readline kind library.")
    (license license:expat)))

(define-public go-github-com-ianlancetaylor-demangle
  (package
    (name "go-github-com-ianlancetaylor-demangle")
    (version "0.0.0-20220517205856-0058ec4f073c")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ianlancetaylor/demangle")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "00xfkjr48v96qwsi68fak0chskks0iq5yvpigx8n373z857xbgdf"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ianlancetaylor/demangle"))
    (home-page "https://github.com/ianlancetaylor/demangle")
    (synopsis "github.com/ianlancetaylor/demangle")
    (description
     "Package demangle defines functions that demangle GCC/LLVM C++ and Rust symbol
names.  This package recognizes names that were mangled according to the C++ ABI
defined at
@@url{http://codesourcery.com/cxx-abi/,http://codesourcery.com/cxx-abi/} and the
Rust ABI defined at
@@url{https://rust-lang.github.io/rfcs/2603-rust-symbol-name-mangling-v0.html,https://rust-lang.github.io/rfcs/2603-rust-symbol-name-mangling-v0.html}")
    (license license:bsd-3)))

(define-public go-github-com-google-pprof
  (package
    (name "go-github-com-google-pprof")
    (version "0.0.0-20221219190121-3cb0bae90811")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/google/pprof")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0krq75zbikggcl7z0jq0ga6l4yj6cal66bxr0jckd7s4a45ikb5k"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/google/pprof"))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-ianlancetaylor-demangle" ,go-github-com-ianlancetaylor-demangle)
                         ("go-github-com-chzyer-readline" ,go-github-com-chzyer-readline)))
    (home-page "https://github.com/google/pprof")
    (synopsis "Introduction")
    (description
     "pprof is a tool for collection, manipulation and visualization of performance
profiles.")
    (license license:asl2.0)))

(define-public go-github-com-felixge-fgprof
  (package
    (name "go-github-com-felixge-fgprof")
    (version "0.9.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/felixge/fgprof")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "03q3vnjd13r944y2qvfncs21lfkgmg0y8z14zz0xda1hz490wha3"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/felixge/fgprof"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-google-pprof" ,go-github-com-google-pprof)))
    (home-page "https://github.com/felixge/fgprof")
    (synopsis "🚀 fgprof - The Full Go Profiler")
    (description
     "fgprof is a sampling Go profiler that allows you to analyze On-CPU as well as
[Off-CPU](@@url{http://www.brendangregg.com/offcpuanalysis.html,http://www.brendangregg.com/offcpuanalysis.html})
(e.g. I/O) time together.")
    (license license:expat)))

(define-public go-github-com-pkg-profile
  (package
    (name "go-github-com-pkg-profile")
    (version "1.7.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/pkg/profile")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ifr9gnycjwh7dbvsb5vgs9kzlr548cb4m45zvl8i8lgd3qhppy1"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/pkg/profile"))
    (propagated-inputs `(("go-github-com-felixge-fgprof" ,go-github-com-felixge-fgprof)))
    (home-page "https://github.com/pkg/profile")
    (synopsis "profile")
    (description
     "Package profile provides a simple way to manage runtime/pprof profiling of your
Go application.")
    (license license:bsd-2)))

(define-public go-github-com-jedib0t-go-pretty-v6
  (package
    (name "go-github-com-jedib0t-go-pretty-v6")
    (version "6.4.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jedib0t/go-pretty")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "04lswvv5nd7w82p4pv6yx5jsvffri1n9z1qdsivbja4xk5a50in9"))))
    (build-system go-build-system)
    (arguments
     (list #:import-path "github.com/jedib0t/go-pretty/v6"
           #:phases #~(modify-phases %standard-phases
                        (replace 'build
                          (lambda* (#:key build-flags #:allow-other-keys)
                            (let ((go-build (@@ (guix build go-build-system) build)))
                              (go-build #:import-path "github.com/jedib0t/go-pretty/v6/list" #:build-flags build-flags)
                              (go-build #:import-path "github.com/jedib0t/go-pretty/v6/progress" #:build-flags build-flags)
                              (go-build #:import-path "github.com/jedib0t/go-pretty/v6/table" #:build-flags build-flags)
                              (go-build #:import-path "github.com/jedib0t/go-pretty/v6/text" #:build-flags build-flags)))))))
    (propagated-inputs `(("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-pkg-profile" ,go-github-com-pkg-profile)
                         ("go-github-com-mattn-go-runewidth" ,go-github-com-mattn-go-runewidth)))
    (home-page "https://github.com/jedib0t/go-pretty")
    (synopsis "go-pretty")
    (description
     "Utilities to prettify console output of tables, lists, progress-bars, text, etc.
 with a heavy emphasis on customization.")
    (license license:expat)))

(define-public go-github-com-mmcdole-gofeed
  (package
    (name "go-github-com-mmcdole-gofeed")
    (version "1.1.3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mmcdole/gofeed")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "04nbhx1arnmxirmhlb9ij7x7wb2pqyzf6ij4nrspizfdbnwx54f7"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/mmcdole/gofeed"))
    (propagated-inputs `(("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-urfave-cli" ,go-github-com-urfave-cli)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-mmcdole-goxpp" ,go-github-com-mmcdole-goxpp)
                         ("go-github-com-json-iterator-go" ,go-github-com-json-iterator-go)
                         ("go-github-com-puerkitobio-goquery" ,go-github-com-puerkitobio-goquery)))
    (home-page "https://github.com/mmcdole/gofeed")
    (synopsis "gofeed")
    (description
     "The @@code{gofeed} library is a robust feed parser that supports parsing both
@@url{https://en.wikipedia.org/wiki/RSS,RSS},
@@url{https://en.wikipedia.org/wiki/Atom_(standard),Atom} and
@@url{https://jsonfeed.org/version/1,JSON} feeds.  The library provides a
universal @@code{gofeed.Parser} that will parse and convert all feed types into
a hybrid @@code{gofeed.Feed} model.  You also have the option of utilizing the
feed specific @@code{atom.Parser} or @@code{rss.Parser} or @@code{json.Parser}
parsers which generate @@code{atom.Feed}, @@code{rss.Feed} and @@code{json.Feed}
respectively.")
    (license license:expat)))

(define-public go-github-com-gocolly-colly
  (package
    (name "go-github-com-gocolly-colly")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gocolly/colly")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1a6brjy0a4pwq2ml3fvz6p7wjmg37rh006i00zxgv9v4vmv7b84d"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gocolly/colly"))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-urfave-cli" ,go-github-com-urfave-cli)
                         ("go-google-golang-org-appengine" ,go-google-golang-org-appengine)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-mmcdole-goxpp" ,go-github-com-mmcdole-goxpp)
                         ("go-github-com-antchfx-htmlquery" ,go-github-com-antchfx-htmlquery)
                         ("go-github-com-antchfx-xmlquery" ,go-github-com-antchfx-xmlquery)
                         ("go-github-com-gobwas-glob" ,go-github-com-gobwas-glob)
                         ("go-github-com-json-iterator-go" ,go-github-com-json-iterator-go)
                         ("go-github-com-saintfish-chardet" ,go-github-com-saintfish-chardet)
                         ("go-github-com-temoto-robotstxt" ,go-github-com-temoto-robotstxt)
                         ("go-github-com-kennygrant-sanitize" ,go-github-com-kennygrant-sanitize)
                         ("go-github-com-puerkitobio-goquery" ,go-github-com-puerkitobio-goquery)))
    (home-page "https://github.com/gocolly/colly")
    (synopsis "Colly")
    (description "Package colly implements a HTTP scraping framework")
    (license license:asl2.0)))

(define-public go-github-com-jawher-mow-cli
  (package
    (name "go-github-com-jawher-mow-cli")
    (version "1.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/jawher/mow.cli")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0wkplzhskrxhs8kcxs8j0b556dhss231idbgcpd1lzsn86y6w3ax"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/jawher/mow.cli"))
    (propagated-inputs `(("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-stretchr-objx" ,go-github-com-stretchr-objx)))
    (home-page "https://github.com/jawher/mow.cli")
    (synopsis "mow.cli")
    (description
     "Package cli provides a framework to build command line applications in Go with
most of the burden of arguments parsing and validation placed on the framework
instead of the user.")
    (license license:expat)))

(define-public go-github-com-gocolly-colly-v2
  (package
    (name "go-github-com-gocolly-colly-v2")
    (version "2.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gocolly/colly")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0a5viya9mrbhp2ba6zjplvb4wc6yk9iivrwsb1q7v3q6viq4ys69"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gocolly/colly/v2"))
    (propagated-inputs `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
                         ("go-google-golang-org-appengine" ,go-google-golang-org-appengine)
                         ("go-golang-org-x-tools" ,go-golang-org-x-tools)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-temoto-robotstxt" ,go-github-com-temoto-robotstxt)
                         ("go-github-com-saintfish-chardet" ,go-github-com-saintfish-chardet)
                         ("go-github-com-kennygrant-sanitize" ,go-github-com-kennygrant-sanitize)
                         ("go-github-com-jawher-mow-cli" ,go-github-com-jawher-mow-cli)
                         ("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)
                         ("go-github-com-gocolly-colly" ,go-github-com-gocolly-colly)
                         ("go-github-com-gobwas-glob" ,go-github-com-gobwas-glob)
                         ("go-github-com-antchfx-xpath" ,go-github-com-antchfx-xpath)
                         ("go-github-com-antchfx-xmlquery" ,go-github-com-antchfx-xmlquery)
                         ("go-github-com-antchfx-htmlquery" ,go-github-com-antchfx-htmlquery)
                         ("go-github-com-andybalholm-cascadia" ,cascadia-1.3)
                         ("go-github-com-puerkitobio-goquery" ,go-github-com-puerkitobio-goquery)))
    (home-page "https://github.com/gocolly/colly")
    (synopsis "Colly")
    (description "Package colly implements a HTTP scraping framework")
    (license license:asl2.0)))

(define-public go-github-com-gosuri-uilive
  (package
    (name "go-github-com-gosuri-uilive")
    (version "0.0.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gosuri/uilive")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0pwxx0w4mv908dascnxkdjq865ks01niqy71imv4kllz0a84zkag"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gosuri/uilive"))
    (home-page "https://github.com/gosuri/uilive")
    (synopsis "uilive")
    (description
     "Package uilive provides a writer that live updates the terminal.  It provides a
buffered io.Writer that is flushed at a timed interval.")
    (license license:expat)))

(define-public go-github-com-gosuri-uilive
  (package
    (name "go-github-com-gosuri-uilive")
    (version "0.0.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gosuri/uilive")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0pwxx0w4mv908dascnxkdjq865ks01niqy71imv4kllz0a84zkag"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gosuri/uilive"))
    (home-page "https://github.com/gosuri/uilive")
    (synopsis "uilive")
    (description
     "Package uilive provides a writer that live updates the terminal.  It provides a
buffered io.Writer that is flushed at a timed interval.")
    (license license:expat)))

(define-public go-github-com-gosuri-uiprogress
  (package
    (name "go-github-com-gosuri-uiprogress")
    (version "0.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gosuri/uiprogress")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1m7rxf71mn8w2yysc8wmf2703avhci6f4nkiijjl1f2cx4kzykck"))))
    (build-system go-build-system)
    (propagated-inputs `(("go-github-com-gosuri-uilive" ,go-github-com-gosuri-uilive)))
    (arguments
     '(#:import-path "github.com/gosuri/uiprogress"))
    (home-page "https://github.com/gosuri/uiprogress")
    (synopsis "uiprogress")
    (description
     "Package uiprogress is a library to render progress bars in terminal applications")
    (license license:expat)))

(define-public go-github-com-antchfx-htmlquery
  (package
    (name "go-github-com-antchfx-htmlquery")
    (version "1.2.5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/antchfx/htmlquery")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1wwp12mkgmfyv4llnwx2sfhmjbk5q368cb2js5ksf7adwg7msjln"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/antchfx/htmlquery"))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-github-com-golang-groupcache" ,go-github-com-golang-groupcache)
                         ("go-github-com-antchfx-xpath" ,go-github-com-antchfx-xpath)))
    (home-page "https://github.com/antchfx/htmlquery")
    (synopsis "htmlquery")
    (description
     "Package htmlquery provides extract data from HTML documents using XPath
expression.")
    (license license:expat)))

(define-public go-github-com-antchfx-xmlquery
  (package
    (name "go-github-com-antchfx-xmlquery")
    (version "1.3.13")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/antchfx/xmlquery")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1kc61qnv5p2kss5v27n1qmdl6clszcx0bbyn05kasmdg82lqqf7j"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/antchfx/xmlquery"))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-github-com-golang-groupcache" ,go-github-com-golang-groupcache)
                         ("go-github-com-antchfx-xpath" ,go-github-com-antchfx-xpath)))
    (home-page "https://github.com/antchfx/xmlquery")
    (synopsis "xmlquery")
    (description
     "Package xmlquery provides extract data from XML documents using XPath
expression.")
    (license license:expat)))

(define-public go-github-com-antchfx-xpath
  (package
    (name "go-github-com-antchfx-xpath")
    (version "1.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/antchfx/xpath")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "03rzaifdgqlzvkyiy4c3iy4xxakyacri8v53rfikqs5bk28cvwbx"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/antchfx/xpath"))
    (home-page "https://github.com/antchfx/xpath")
    (synopsis "XPath")
    (description
     "XPath is Go package provides selecting nodes from XML, HTML or other documents
using XPath expression.")
    (license license:expat)))

(define-public go-github-com-gabriel-vasile-mimetype
  (package
    (name "go-github-com-gabriel-vasile-mimetype")
    (version "1.4.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gabriel-vasile/mimetype")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1skssj7nzszvs61ssqqxf360msnzvsry2xafl6fggp5bczc92rl8"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gabriel-vasile/mimetype"
       #:tests? #f))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)))
    (home-page "https://github.com/gabriel-vasile/mimetype")
    (synopsis "Features")
    (description
     "Package mimetype uses magic number signatures to detect the MIME type of a file.")
    (license license:expat)))

(define-public go-github-com-go-shiori-dom
  (package
    (name "go-github-com-go-shiori-dom")
    (version "0.0.0-20210627111528-4e4722cd0d65")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-shiori/dom")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1kcf9lj9609f6d9r9gymx2kcaslk0g153x5j611wslgyzgphcylh"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/go-shiori/dom"))
    (propagated-inputs `(("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-github-com-andybalholm-cascadia" ,cascadia-1.3)
                         ("go-github-com-gogs-chardet" ,go-github-com-gogs-chardet)))
    (home-page "https://github.com/go-shiori/dom")
    (synopsis "DOM")
    (description
     "DOM is a Go package for manipulating
@@url{https://godoc.org/golang.org/x/net/html,HTML} node.  The methods that
exist in this package has similar name and purpose as the DOM manipulation
methods in JS, which make it useful when porting code from JS to Go.  Currently
it used in @@url{https://github.com/go-shiori/warc,(code warc)} and
@@url{https://github.com/go-shiori/go-readability,(code go-readability)}.")
    (license license:expat)))

(define-public go-github-com-gofrs-uuid
  (package
    (name "go-github-com-gofrs-uuid")
    (version "4.3.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/gofrs/uuid")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1szkrxs6s3b651mxmnr4a4qf75788lb3b65i0sv31yvqblvjs9y6"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/gofrs/uuid"))
    (home-page "https://github.com/gofrs/uuid")
    (synopsis "UUID")
    (description
     "Package uuid provides implementations of the Universally Unique Identifier
(UUID), as specified in RFC-4122 and the Peabody RFC Draft (revision 03).")
    (license license:expat)))

(define-public go-github-com-golang-groupcache
  (package
    (name "go-github-com-golang-groupcache")
    (version "0.0.0-20210331224755-41bb18bfe9da")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/golang/groupcache")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "07amgr8ji4mnq91qbsw2jlcmw6hqiwdf4kzfdrj8c4b05w4knszc"))))
    (build-system go-build-system)
    (propagated-inputs `(("go-google-golang-org-protobuf" ,go-google-golang-org-protobuf)))
    (arguments
     '(#:import-path "github.com/golang/groupcache"))
    (home-page "https://github.com/golang/groupcache")
    (synopsis "groupcache")
    (description
     "Package groupcache provides a data loading mechanism with caching and
de-duplication that works across a set of peer processes.")
    (license license:asl2.0)))

(define-public go-github-com-inconshreveable-mousetrap
  (package
    (name "go-github-com-inconshreveable-mousetrap")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/inconshreveable/mousetrap")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "14gjpvwgx3hmbd92jlwafgibiak2jqp25rq4q50cq89w8wgmhsax"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/inconshreveable/mousetrap"))
    (home-page "https://github.com/inconshreveable/mousetrap")
    (synopsis "mousetrap")
    (description "mousetrap is a tiny library that answers a single question.")
    (license license:asl2.0)))

(define-public go-github-com-json-iterator-go
  (package
    (name "go-github-com-json-iterator-go")
    (version "1.1.12")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/json-iterator/go")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1c8f0hxm18wivx31bs615x3vxs2j3ba0v6vxchsjhldc8kl311bz"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/json-iterator/go"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-modern-go-reflect2" ,go-github-com-modern-go-reflect2)
                         ("go-github-com-modern-go-concurrent" ,go-github-com-modern-go-concurrent)
                         ("go-github-com-google-gofuzz" ,go-github-com-google-gofuzz)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/json-iterator/go")
    (synopsis "Benchmark")
    (description
     "Package jsoniter implements encoding and decoding of JSON as defined in
@@url{https://rfc-editor.org/rfc/rfc4627.html,RFC 4627} and provides interfaces
with identical syntax of standard lib encoding/json.  Converting from
encoding/json to jsoniter is no more than replacing the package with jsoniter
and variable type declarations (if any).  jsoniter interfaces gives 100%
compatibility with code using standard lib.")
    (license license:expat)))

(define-public go-github-com-kennygrant-sanitize
  (package
    (name "go-github-com-kennygrant-sanitize")
    (version "1.2.4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/kennygrant/sanitize")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "06f2ljnic3215ihzc5px1q25548ijpixhmn4537gf507n1sxg7iq"))))
    (build-system go-build-system)
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)))
    (arguments
     '(#:import-path "github.com/kennygrant/sanitize"))
    (home-page "https://github.com/kennygrant/sanitize")
    (synopsis "sanitize")
    (description "Package sanitize provides functions for sanitizing text.")
    (license license:bsd-3)))

(define-public go-github-com-mmcdole-goxpp
  (package
    (name "go-github-com-mmcdole-goxpp")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/mmcdole/goxpp")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "13fqj3r6hnqy86lz1kscjv2i38f2g716gll0zqvy845rbgcmnpgn"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/mmcdole/goxpp"))
    (propagated-inputs `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/mmcdole/goxpp")
    (synopsis "goxpp")
    (description
     "The @@code{goxpp} library is an XML parser library that is loosely based on the
@@url{http://www.xmlpull.org/v1/download/unpacked/doc/quick_intro.html,Java
XMLPullParser}.  This library allows you to easily parse arbitrary XML content
using a pull parser.  You can think of @@code{goxpp} as a lightweight wrapper
around Go's XML @@code{Decoder} that provides a set of functions that make it
easier to parse XML content than using the raw decoder itself.")
    (license license:expat)))

(define-public go-github-com-modern-go-concurrent
  (package
    (name "go-github-com-modern-go-concurrent")
    (version "0.0.0-20180306012644-bacd9c7ef1dd")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/modern-go/concurrent")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0s0fxccsyb8icjmiym5k7prcqx36hvgdwl588y0491gi18k5i4zs"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/modern-go/concurrent"))
    (home-page "https://github.com/modern-go/concurrent")
    (synopsis "concurrent")
    (description
     "because sync.Map is only available in go 1.9, we can use concurrent.Map to make
code portable")
    (license license:asl2.0)))

(define-public go-github-com-modern-go-reflect2
  (package
    (name "go-github-com-modern-go-reflect2")
    (version "1.0.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/modern-go/reflect2")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "05a89f9j4nj8v1bchfkv2sy8piz746ikj831ilbp54g8dqhl8vzr"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/modern-go/reflect2"))
    (home-page "https://github.com/modern-go/reflect2")
    (synopsis "reflect2")
    (description "reflect api that avoids runtime reflect.Value cost")
    (license license:asl2.0)))

(define-public go-github-com-saintfish-chardet
  (package
    (name "go-github-com-saintfish-chardet")
    (version "0.0.0-20120816061221-3af4cd4741ca")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/saintfish/chardet")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0czh50md64k9lbllayq0asir3174saxb88yzxrh640yhfxd98pcb"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/saintfish/chardet"))
    (home-page "https://github.com/saintfish/chardet")
    (synopsis "chardet")
    (description "Package chardet ports character set detection from ICU.")
    (license license:expat)))

(define-public go-github-com-temoto-robotstxt
  (package
    (name "go-github-com-temoto-robotstxt")
    (version "1.1.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/temoto/robotstxt")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0a1bbym8gr9wyanh2br6hmxhdbqpfdr3nb56b4bvglad19fv2fpg"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/temoto/robotstxt"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/temoto/robotstxt")
    (synopsis #f)
    (description
     "Package robotstxt implements the robots.txt Exclusion Protocol as specified in
@@url{http://www.robotstxt.org/wc/robots.html,http://www.robotstxt.org/wc/robots.html}
with various extensions.")
    (license license:expat)))

(define-public go-github-com-vincent-petithory-dataurl
  (package
    (name "go-github-com-vincent-petithory-dataurl")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/vincent-petithory/dataurl")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "05vdlrqpshrcvcs2z5jga17nxglba4nv8y1z7a2w1nc4rj76i3k7"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/vincent-petithory/dataurl"))
    (home-page "https://github.com/vincent-petithory/dataurl")
    (synopsis "Data URL Schemes for Go")
    (description
     "Package dataurl parses Data URL Schemes according to
@@url{https://rfc-editor.org/rfc/rfc2397.html,RFC 2397}
(@@url{http://tools.ietf.org/html/rfc2397,http://tools.ietf.org/html/rfc2397}).")
    (license license:expat)))

(define-public go-google-golang-org-appengine
  (package
    (name "go-google-golang-org-appengine")
    (version "1.6.7")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/golang/appengine")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1wkipg7xxc0ha5p6c3bj0vpgq38l18441n5l6zxdhx0gzvz5z1hs"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "google.golang.org/appengine"))
    (propagated-inputs (list go-golang-org-x-text go-golang-org-x-net
                             go-google-golang-org-protobuf))
    (home-page "https://google.golang.org/appengine")
    (synopsis "Go App Engine packages")
    (description
     "Package appengine provides basic functionality for Google App Engine.")
    (license license:asl2.0)))

(define-public papeer
  (package
    (name "papeer")
    (version "0.5.6")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/lapwat/papeer")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1rz7g7aprd4yp769vkqxqacfs2lmlws16ymva057vxk1wk1r5q32"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/lapwat/papeer"))
    (propagated-inputs (list go-google-golang-org-protobuf
                             go-google-golang-org-appengine
                             go-golang-org-x-text
                             go-golang-org-x-sys
                             go-golang-org-x-net
                             go-github-com-vincent-petithory-dataurl
                             go-github-com-temoto-robotstxt
                             go-github-com-spf13-pflag
                             go-github-com-sirupsen-logrus
                             go-github-com-saintfish-chardet
                             go-github-com-rivo-uniseg
                             go-github-com-modern-go-reflect2
                             go-github-com-modern-go-concurrent
                             (patch-cascadia go-github-com-mmcdole-goxpp)
                             go-github-com-mattn-go-runewidth
                             go-github-com-mattn-go-isatty
                             go-github-com-kennygrant-sanitize
                             go-github-com-json-iterator-go
                             go-github-com-inconshreveable-mousetrap
                             go-google-golang-org-protobuf
                             go-github-com-golang-groupcache
                             go-github-com-gogs-chardet
                             go-github-com-gofrs-uuid
                             go-github-com-gobwas-glob
                             go-github-com-go-shiori-dom
                             go-github-com-gabriel-vasile-mimetype
                             go-github-com-antchfx-xpath
                             go-github-com-antchfx-xmlquery
                             go-github-com-antchfx-htmlquery
                             cascadia-1.3
                             go-github-com-gosuri-uiprogress
                             go-github-com-gosuri-uilive
                             (patch-cascadia go-github-com-gocolly-colly-v2)
                             (patch-cascadia go-github-com-puerkitobio-goquery)
                             go-github-com-spf13-cobra
                             (patch-cascadia go-github-com-mmcdole-gofeed)
                             go-github-com-jedib0t-go-pretty-v6
                             go-github-com-go-shiori-go-readability
                             go-github-com-bmaupin-go-epub
                             (patch-cascadia
                              go-github-com-johanneskaufmann-html-to-markdown)))
    (home-page "https://github.com/lapwat/papeer")
    (synopsis #f)
    (description #f)
    (license #f)))
