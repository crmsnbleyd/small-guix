;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix packages guile-xyz)
  #:use-module (gnu packages guile-xyz)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public guile-g-golf.git
  (let ((version "0.8.0-a.5")
        (revision "0")
        (commit "412074362489ff31cb326d5fdb0adb3a2eb5bd90"))
    (package (inherit guile-g-golf)
      (name "guile-g-golf.git")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://git.savannah.gnu.org/git/g-golf.git")
               (commit (string-append commit))))
         (file-name (git-file-name name version))
         (sha256
          (base32 "17n09xzqlp4dwafaz371k6pzzal92gnjv49z2gqlnc37nkgz9268")))))))
