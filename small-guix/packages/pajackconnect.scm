;;; Copyright © 2020, 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix packages pajackconnect)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy)
  #:use-module (guix utils))

(define-public pajackconnect
  (let ((version "0.0.0")
        (revision "0")
        (commit "d43276df467c694fbbf72a74040dc1a29e86f40b"))
    (package
      (name "pajackconnect")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/brummer10/pajackconnect.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1zvyyhvqiz8dirajgpb0r8ll0ih80k02bxnxyh98fzfgdmjpji70"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan '(("pajackconnect" "bin/"))))
      (home-page "https://github.com/brummer10/pajackconnect")
      (synopsis "Make JACK work with PulseAudio")
      (description
       "This script is intended to be invoked via @code{QjackCtl}
to start up and shut down JACK on a system running PulseAudio.  It handles
the necessary setup to make the two work together, so PulseAudio clients get
transparently routed through JACK while the latter is running, or if PulseAudio
is suspend by pasuspender, do nothing.

If you are using @code{jack-dbus}, and have @code{pulseaudio-module-jack}
installed, you already have a similar functionality and this script is not
required.")
      (license license:cc0))))
