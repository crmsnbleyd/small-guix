;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019, 2021 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (small-guix packages omnetpp)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages image)
  #:use-module (gnu packages java)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages wget)
  #:use-module (gnu packages xorg)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public blt
  (package
    (name "blt")
    (version "3.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.code.sf.net/p/blt/src")
                    (commit "b8cdc6c2cd8edba06ad53a5d56e961d35b07af28")))
              (sha256
               (base32
                "0nify0jaxdw4s2s9ws44hbrf6v1r6lycxlxm07pf3lksfvqgywk6"))))
    (build-system gnu-build-system)
    (inputs `(("libjpeg-turbo" ,libjpeg-turbo)
              ("libssh2" ,libssh2)
              ("libtiff" ,libtiff)
              ("libxft" ,libxft)
              ("libxpm" ,libxpm)
              ("libxrandr" ,libxrandr)
              ("mariadb" ,mariadb)
              ("sqlite" ,sqlite)
              ("tcl" ,tcl)
              ("tk" ,tk)))
    (native-inputs `(("freetype" ,freetype)
                     ("pkg-config" ,pkg-config)))
    (arguments
     `(#:tests? #f
       #:configure-flags (let ((out (assoc-ref %outputs "out"))
                               (tcl (assoc-ref %build-inputs "tcl"))
                               (tk (assoc-ref %build-inputs "tk"))
                               (freetype (assoc-ref %build-inputs "freetype")))
                           (list "--with-x"
                                 (string-append "--x-includes=" out "/include")
                                 (string-append "--x-libraries=" out "/lib")
                                 (string-append "--mandir=" out "/share/man")
                                 (string-append "--with-tcl=" tcl "/lib")
                                 (string-append "--with-tclincdir=" tcl
                                                "/include")
                                 (string-append "--with-tklibdir=" tk "/lib")
                                 (string-append "--with-tkincdir=" tk
                                                "/include")
                                 (string-append "--with-freetype2libdir="
                                                freetype "/lib")
                                 (string-append "--with-freetype2incdir="
                                                freetype "/include")))
       #:phases (modify-phases %standard-phases
                  (delete 'validate-runpath)
                  (add-before 'install 'add-destdir
                    (lambda _
                      (substitute* "Makefile"
                        (("bltConfig.sh $(libdir)")
                         "bltConfig.sh $(DESTDIR)$(libdir)"))
                      (map (lambda file
                             (substitute* file
                               (("$(destdir)")
                                "$(DESTDIR)$(destdir)")) #t)
                           '("library/dd_protocols/Makefile"
                             "library/palettes/Makefile")) #t)))))
    (synopsis "Extension to the Tk toolkit, adding new widgets
and miscellaneous commands")
    (description
     "The BLT Toolkit is an extension to Tcl and Tk.  It adds new commands,
geometry managers, and widgets to the Tcl interpreter.  Included widgets are 2D graph,
barchart, stripchart, tab notebook, and tree viewer.")
    (license license:bsd-3)
    (home-page "http://blt.sourceforge.net/")))

(define-public omnetpp
  (package
    (name "omnetpp")
    (version "5.5.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/omnetpp/omnetpp.git")
                    (commit (string-append name "-" version))))
              (sha256
               (base32
                "0fsm33aaq0x19df3cs0r4m41kyis35xq5k69gshbxayixym4p5rh"))))
    (build-system gnu-build-system)
    (inputs `(("blt" ,blt)
              ("doxygen" ,doxygen)
              ("graphviz" ,graphviz)
              ("libpcap" ,libpcap)
              ("openmpi" ,openmpi)
              ("openscenegraph" ,openscenegraph)
              ("qtbase" ,qtbase)
              ("tcl" ,tcl)
              ("tk" ,tk)))
    (native-inputs `(("bison" ,bison)
                     ("flex" ,flex)
                     ("libxml2" ,libxml2)
                     ("perl" ,perl)
                     ("python" ,python-wrapper)
                     ("pkg-config" ,pkg-config)
                     ("wget" ,wget)))
    (propagated-inputs `(("gcc" ,gcc)
                         ("glibc" ,glibc)
                         ("binutils" ,binutils)
                         ("openjdk9:jdk" ,openjdk9 "jdk")
                         ("python" ,python)))
    (native-search-paths
     (list (search-path-specification
            (variable "LD_LIBRARY_PATH")
            (files '("lib")))
           (search-path-specification
            (variable "OMNETPP_IMAGE_PATH")
            (files '("images")))
           (search-path-specification
            (variable "OMNETPP_TKENV_DIR")
            (files '("src/tkenv")))))
    (arguments
     `(#:tests? #f
       #:configure-flags (list (string-append "--prefix="
                                              (assoc-ref %outputs "out"))
                               (string-append "--libdir="
                                              (assoc-ref %outputs "out")
                                              "/lib")
                               (string-append "--libexecdir="
                                              (assoc-ref %outputs "out")
                                              "/lib"))
       #:make-flags '("MODE=release")
       #:phases (modify-phases %standard-phases
                  (add-before 'configure 'patch-configure
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out"))
                             (here (getcwd))
                             (conf "configure")
                             (user-conf (string-append conf ".user")))
                        ;; symlink build dir to out to trick OMNET
                        ;; into thinking it is being built in its install directory
                        (symlink here out)
                        (copy-file (string-append user-conf ".dist") user-conf)
                        (system (string-append "chmod -R 744 " out))
                        (substitute* user-conf
                          (("WITH_OSGEARTH=yes")
                           "WITH_OSGEARTH=no"))
                        (let ((output-port (open-file user-conf "a")))
                          (display (string-append "OMNETPP_ROOT=\"" out "\"")
                                   output-port)
                          (newline output-port)
                          (close output-port))
                        (substitute* conf
                          (("if test ! -x /usr/bin/perl; then")
                           (string-append "if test ! -x "
                                          (which "perl") "; then"))
                          (("ac_configure_args=")
                           "ac_configure_args=$(echo $ac_configure_args | sed s/\\'//g)")
                          (("-rpath,\\.")
                           (string-append "-rpath,." " -Wl,-rpath," out)))
                        (system ". ./setenv -f")
                        (setenv "PATH"
                                (string-append out "/bin:"
                                               (getenv "PATH")))
                        (setenv "LD_LIBRARY_PATH"
                                (string-append out "/lib"))
                        #t)))
                  (add-before 'build 'patch-makefile
                    (lambda _
                      (substitute* '("Makefile" "src/utils/Makefile")
                        (("#!/bin/sh")
                         (string-append "#!"
                                        (which "sh"))))
                      (substitute* "src/utils/Makefile"
                        (("\\$\\(Q\\)echo \\$\\(WISH\\) >\\$\\(HOME\\)/\\.wishname")
                         "")) #t))
                  (replace 'install
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((here (getcwd))
                             (out (assoc-ref outputs "out"))
                             (bin (string-append out "/bin"))
                             (samples (string-append out "/samples"))
                             (omnetpp-guix-home "$HOME/.omnetpp-guix")
                             (error-log (string-append omnetpp-guix-home
                                                       "/error.log")))
                        (delete-file out) ;symlink to the build directory
                        (mkdir-p out)
                        (copy-recursively here out)
                        (map patch-shebang
                             (find-files bin))
                        (substitute* (string-append bin "/opp_test")
                          (("/usr/bin/env python")
                           (which "python"))
                          (("#! /bin/sh")
                           (which "sh")))
                        (substitute* (string-append bin "/omnetpp")
                          (("\\$IDEDIR/error\\.log")
                           error-log)
                          (("IDEDIR=`dirname \\$0`/\\.\\./ide")
                           (string-append "IDEDIR="
                            out
                            "/ide"
                            "\n\nif test ! -f "
                            error-log
                            "; then"
                            "\n    echo 'First run, creating ~/.omnetpp-guix directory'"
                            "\n    mkdir -p "
                            omnetpp-guix-home
                            "/samples/"
                            "\n    cp -r "
                            samples
                            " "
                            omnetpp-guix-home
                            "/samples/"
                            "\n    touch "
                            error-log
                            "\n    chmod -R 777 "
                            omnetpp-guix-home
                            "\nfi\n"))
                          (("-Dosgi\\.instance\\.area\\.default=\\$IDEDIR/\\.\\./samples")
                           (string-append "-Dosgi.instance.area.default="
                                          omnetpp-guix-home "/samples")))) #t))
                  (delete 'validate-runpath))))
    (home-page "https://omnetpp.org/")
    (synopsis "")
    (description "@code{fenics}")
    (license license:lgpl3+)))
