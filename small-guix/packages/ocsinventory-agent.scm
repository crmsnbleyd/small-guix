;;; Copyright © 2021 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix packages ocsinventory-agent)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages disk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages perl-check)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xml)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system perl)
  #:use-module (guix utils))

(define-public perl-proc-processtable
  (package
    (name "perl-proc-processtable")
    (version "0.59")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/J/JW/JWB/Proc-ProcessTable-"
                    version ".tar.gz"))
              (sha256
               (base32
                "0v30jijk2zwrwwvyrlhkj2ly40pl9q5l6xgv777a0dccsxa51k7q"))))
    (build-system perl-build-system)
    (home-page "https://metacpan.org/release/Proc-ProcessTable")
    (synopsis "Perl extension to access the unix process table")
    (description
     "This package provides the @code{Proc::ProcessTable} module, a first crack
at providing a consistent interface to Unix process table information.")
    (license license:artistic2.0)))

(define-public perl-proc-daemon
  (package
    (name "perl-proc-daemon")
    (version "0.23")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/A/AK/AKREAL/Proc-Daemon-"
                    version ".tar.gz"))
              (sha256
               (base32
                "0scv11s9847b761vvb9vyk61br9m11cfwz69mg5k3d28g5dvih1l"))
              (modules '((guix build utils)))
              (snippet '(begin
                          ;; FIXME: Some tests fail.
                          (delete-file "t/02_testmodule.t")
                          (delete-file "t/03_taintmode.t") #t))))
    (build-system perl-build-system)
    (native-inputs `(("perl-proc-processtable" ,perl-proc-processtable)))
    (home-page "https://metacpan.org/release/Proc-Daemon")
    (synopsis "Run Perl program(s) as a daemon process")
    (description
     "This package provides the @code{Proc:Daemon} module.  It can be
sed by a Perl program to initialize itself as a daemon or to execute
(@code{exec}) a system command as daemon.  You can also check the status
of the daemon (alive or dead) and you can kill the daemon.")
    (license license:perl-license)))

(define-public perl-proc-pid-file
  (package
    (name "perl-proc-pid-file")
    (version "1.29")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/D/DM/DMITRI/Proc-PID-File-"
                    version ".tar.gz"))
              (sha256
               (base32
                "196lbhszv4lri91d19jhb7a2yvq5n98jl35pd20ksb8qvx4xmkiv"))))
    (build-system perl-build-system)
    (arguments
     ;; FIXME: Some tests hang indefinitely.
     `(#:tests? #f))
    (propagated-inputs `(("procps" ,procps)))
    (home-page "https://metacpan.org/release/Proc-PID-File")
    (synopsis "Perl module to manage PID files")
    (description
     "This package provides the @code{Proc::PID::File} Perl module.  It is
useful for writers of daemons and other processes that need to tell whether
they are already running, in order to prevent multiple process instances.  The
module accomplishes this via *nix-style pidfiles, which are files that store
a process identifier.")
    (license license:gpl2+)))

(define-public perl-parse-edid
  (package
    (name "perl-parse-edid")
    (version "1.0.7")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/G/GR/GROUSSE/Parse-EDID-"
                    version ".tar.gz"))
              (sha256
               (base32
                "1wavcxfic3kr18cndqwwv7z5xw45z7hb19ad5cm8l69jb880zp0s"))))
    (build-system perl-build-system)
    (native-inputs `(("perl-test-warn" ,perl-test-warn)))
    (home-page "https://metacpan.org/release/Parse-EDID")
    (synopsis "Extended display identification data (EDID) parser")
    (description
     "This package provides the @code{Parse::EDID} module.  It implements
some function to parse Extended Display Identification Data binary data
structures.")
    (license license:gpl3)))

(define-public perl-net-ip
  (package
    (name "perl-net-ip")
    (version "1.26")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://cpan/authors/id/M/MA/MANU/Net-IP-"
                    version ".tar.gz"))
              (sha256
               (base32
                "0ffn2xqqbkfi7v303sp5dwgbv36jah3vg8r4nxhxfiv60vric3q4"))))
    (build-system perl-build-system)
    (home-page "https://metacpan.org/release/Net-IP")
    (synopsis "Perl extension for manipulating IPv4/IPv6 addresses")
    (description
     "This package provides the @code{Net::IP} Perl module.  It implements
functions to deal with IPv4/IPv6 addresses.  The module can be used as a class,
allowing the user to instantiate IP objects, which can be single IP addresses,
prefixes, or ranges of addresses.  There is also a procedural way of accessing
most of the functions.  Most subroutines can take either IPv4 or IPv6
addresses transparently.")
    (license (list license:artistic2.0 license:gpl1+ license:expat))))

(define-public perl-io-interactive-tiny
  (package
    (name "perl-io-interactive-tiny")
    (version "0.2")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/D/DM/DMUEY/IO-Interactive-Tiny-"
                    version ".tar.gz"))
              (sha256
               (base32
                "1yd5bv4kya08h2sx5gjcrvxpig5inw92bkgm8mw39r670mjnkh25"))))
    (build-system perl-build-system)
    (native-inputs `(("perl-module-build" ,perl-module-build)))
    (home-page "https://metacpan.org/release/IO-Interactive-Tiny")
    (synopsis "Implements @code{is_interactive()} without large deps")
    (description
     "This package provides the @code{IO::Interactive-Tiny} Perl module.
It implements a subset of @code{IO::Interactive}’s functionality in the
form of only having @code{is_interactive()}.")
    (license (package-license perl))))

(define-public perl-crypt-ssleay
  (package
    (name "perl-crypt-ssleay")
    (version "0.72")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/N/NA/NANIS/Crypt-SSLeay-"
                    version ".tar.gz"))
              (sha256
               (base32
                "1s7zm6ph37kg8jzaxnhi4ff4snxl7mi5h14arxbri0kp6s0lzlzm"))))
    (build-system perl-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (add-before 'configure 'set-perl-search-path
                    (lambda _
                      ;; Work around "dotless @INC" build failure.
                      (setenv "PERL5LIB"
                              (string-append (getcwd) ":"
                                             (getenv "PERL5LIB"))) #t))
                  (delete 'check))))
    (propagated-inputs `(("perl-lwp-protocol-https" ,perl-lwp-protocol-https)))
    (native-inputs `(("openssl" ,openssl)
                     ("perl-path-class" ,perl-path-class)
                     ("perl-try-tiny" ,perl-try-tiny)))
    (home-page "https://metacpan.org/release/Crypt-SSLeay")
    (synopsis "OpenSSL support for LWP")
    (description
     "This package provides the @code{Crypt::SSLeay} Perl module.  It implements
support for the HTTPS protocol under LWP, so that an @code{LWP::UserAgent} can
make HTTPS GET, HEAD and POST requests.")
    (license license:artistic2.0)))

(define-public perl-net-snmp
  (package
    (name "perl-net-snmp")
    (version "6.0.1")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/D/DT/DTOWN/Net-SNMP-v" version
                    ".tar.gz"))
              (sha256
               (base32
                "0hdpn1cw52x8cw24m9ayzpf4rwarm0khygn1sv3wvwxkrg0pphql"))))
    (build-system perl-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
                  (add-before 'check 'remove-network-dependent-tests
                    (lambda _
                      (delete-file "t/dsp.t")
                      (delete-file "t/mp.t") #t)))))
    (native-inputs `(("perl-module-build" ,perl-module-build)))
    (propagated-inputs `(("perl-crypt-des" ,perl-crypt-des)
                         ("perl-crypt-rijndael" ,perl-crypt-rijndael)
                         ("perl-digest-hmac" ,perl-digest-hmac)
                         ("perl-digest-sha1" ,perl-digest-sha1)
                         ("perl-socket6" ,perl-socket6)))
    (home-page "https://metacpan.org/release/Net-SNMP")
    (synopsis "Object oriented interface to SNMP")
    (description
     "This package provides the @code{Net::SNMP} Perl module.
It implements an object oriented interface to the Simple Network Management
Protocol.  Perl applications can use the module to retrieve or update
information on a remote host using the SNMP protocol.")
    (license license:perl-license)))

(define-public perl-test-useallmodules
  (package
    (name "perl-test-useallmodules")
    (version "0.17")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/I/IS/ISHIGAKI/Test-UseAllModules-"
                    version ".tar.gz"))
              (sha256
               (base32
                "0295g9f1k8b9nxbm846b1nraajd0bq9x788afv1bzf3ap7l2y7x7"))))
    (build-system perl-build-system)
    (home-page "https://metacpan.org/release/Test-UseAllModules")
    (synopsis "@code{do use_ok()} for all the MANIFESTed modules")
    (description
     "This package provides the @code{Test::UseAllModules} Perl module.")
    (license license:perl-license)))

(define-public perl-net-netmask
  (package
    (name "perl-net-netmask")
    (version "1.9104")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/J/JM/JMASLAK/Net-Netmask-"
                    version ".tar.gz"))
              (sha256
               (base32
                "17li2svymz49az35xl6galp4b9qcnb985gzklhikkvkn9da6rz3y"))))
    (build-system perl-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
                  (add-before 'check 'remove-failing-tests
                    (lambda _
                      ;; FIXME: Some tests fail with:
                      ;; Parse errors: No plan found in TAP output
                      (delete-file "t/ipv6_cannonical.t")
                      (delete-file "t/ipv6_raw2ascii.t") #t)))))
    (native-inputs `(("perl-test-useallmodules" ,perl-test-useallmodules)
                     ("perl-test2-suite" ,perl-test2-suite)))
    (propagated-inputs `(("perl-anyevent" ,perl-anyevent)))
    (home-page "https://metacpan.org/release/Net-Netmask")
    (synopsis "Understand and manipulate IP netmasks")
    (description "This package provides the @code{Net::Netmask} Perl module.
It can parse, manipulate and lookup IP network blocks.")
    (license license:perl-license)))

(define-public ocsinventory-agent
  (package
    (name "ocsinventory-agent")
    (version "2.8.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/OCSInventory-NG/UnixAgent")
                    (commit (string-append "v" version))))
              (sha256
               (base32
                "0r79p4y1djjp0b4fxhmlvlwmy9vbqd6j5bg43jkr4amxyjkk1r8b"))
              (file-name (git-file-name name version))))
    (build-system perl-build-system)
    (arguments
     `(#:modules ((guix build perl-build-system)
                  (guix build utils)
                  (ice-9 match)
                  (srfi srfi-26))
       #:imported-modules (,@%perl-build-system-modules (guix build utils)
                           (ice-9 match)
                           (srfi srfi-26))
       #:phases (modify-phases %standard-phases
                  (add-after 'build 'disable-postinstall-script
                    (lambda _
                      (delete-file "run-postinst") #t))
                  (add-after 'install 'find-itself-and-dependencies
                    ;; Fix run-time 'Can't locate Ocsinventory/Agent.pm in @INC' failure.
                    (lambda* (#:key inputs outputs #:allow-other-keys)
                      (use-modules (guix build utils)
                                   (ice-9 match)
                                   (srfi srfi-26))
                      (let* ((out (assoc-ref outputs "out"))
                             (bin (string-append out "/bin")))
                        (with-directory-excursion bin
                          (for-each (lambda (program)
                                      (wrap-program program
                                                    `("PATH" ":" =
                                                      ("$PATH" ,@(map (lambda 
                                                                              (input)
                                                                        (match input
                                                                          ((name . store) (let 
                                                                                               (
                                                                                                (store-append
                                                                                                 (cut
                                                                                                  string-append
                                                                                                  store
                                                                                                  <>)))
                                                                                            (cond
                                                                                              
                                                                                              (
                                                                                               (member
                                                                                                name
                                                                                                '
                                                                                                ("util-linux"))
                                                                                               
                                                                                               (string-append
                                                                                                (store-append
                                                                                                 "/bin")
                                                                                                ":"
                                                                                                
                                                                                                (store-append
                                                                                                 "/sbin")))
                                                                                              
                                                                                              (
                                                                                               (member
                                                                                                name
                                                                                                '
                                                                                                ("dmidecode"
                                                                                                 "iproute2"))
                                                                                               
                                                                                               (store-append
                                                                                                "/sbin"))
                                                                                              
                                                                                              (else
                                                                                               (store-append
                                                                                                "/bin")))))))
                                                                      inputs)))
                                                    `("PERL5LIB" ":" =
                                                      ,(cons (string-append
                                                              out
                                                              "/lib/perl5/site_perl")
                                                             (delete ""
                                                                     (map (match-lambda
                                                                            (((? (cut string-prefix? "perl-" <>) name) . dir)
                                                                             (string-append
                                                                              dir
                                                                              "/lib/perl5/site_perl"))
                                                                            (_
                                                                             ""))
                                                                      inputs))))))
                                    (find-files "." ".*")) #t)))))))
    (native-inputs `(("perl-crypt-ssleay" ,perl-crypt-ssleay)
                     ("perl-digest-md5" ,perl-digest-md5)
                     ("perl-io-socket-ssl" ,perl-io-socket-ssl)
                     ("perl-libwww" ,perl-libwww)
                     ("perl-lwp-protocol-https" ,perl-lwp-protocol-https)
                     ("perl-module-install" ,perl-module-install)
                     ("perl-net-ip" ,perl-net-ip)
                     ("perl-net-netmask" ,perl-net-netmask)
                     ("perl-net-snmp" ,perl-net-snmp)
                     ("perl-parse-edid" ,perl-parse-edid)
                     ("perl-proc-daemon" ,perl-proc-daemon)
                     ("perl-proc-pid-file" ,perl-proc-pid-file)
                     ("perl-xml-simple" ,perl-xml-simple)))
    (inputs `(("dmidecode" ,dmidecode)
              ("fdisk" ,fdisk)
              ("hdparm" ,hdparm)
              ("pciutils" ,pciutils)
              ("util-linux" ,util-linux)))
    (home-page "https://ocsinventory-ng.org")
    (synopsis "OCS unified agent for Unix operating systems")
    (description
     "Ocsinventory-Agent is an agent for ocsinventory NG.  It supports Linux,
Solaris and AIX.")
    (license license:perl-license)))

(define-public ocsinventory-agent-2.6
  (package
    (inherit ocsinventory-agent)
    (name "ocsinventory-agent")
    (version "2.6.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/OCSInventory-NG/UnixAgent")
                    (commit (string-append "v" version))))
              (sha256
               (base32
                "03xgpmh94z20hjk17r2xqscsi61sbfk5fri177fnl2w5xz639jky"))
              (file-name (git-file-name name version))))))
