;;; Copyright © 2021 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix packages gamehub)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages games)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages xml)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system meson)
  #:use-module (guix utils))

(define-public gamehub
  (package
    (name "gamehub")
    (version "0.16.0-1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/tkashkin/GameHub")
                    (commit (string-append version "-master"))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1zwhms8bdzxxqs3j5y74iva158qgpsfzysgwmh7n14jd4wdnmjj1"))))
    (build-system meson-build-system)
    (arguments
     `(#:glib-or-gtk? #t
       #:configure-flags '("-Ddistro=generic")
       #:phases (modify-phases %standard-phases
                  ;; (add-after 'unpack 'skip-gtk-update-icon-cache
                  ;; ;; Don't create 'icon-theme.cache'.
                  ;; (lambda _
                  ;; (substitute* "build-aux/meson/postinstall.py"
                  ;; (("gtk-update-icon-cache") "true"))
                  ;; #t))
                  ;; (add-after 'unpack 'skip-update-desktop-database
                  ;; ;; Don't update desktop file database.
                  ;; (lambda _
                  ;; (substitute* "build-aux/meson/postinstall.py"
                  ;; (("update-desktop-database") "true"))
                  ;; #t))
                  )))
    (inputs `(("glib" ,glib)
              ("gtk3" ,gtk+)
              ("libgee" ,libgee)
              ("libmanette" ,libmanette)
              ("libsoup" ,libsoup)
              ("libx11" ,libx11)
              ("libxtst" ,libxtst)
              ("json-glib" ,json-glib)
              ("sqlite" ,sqlite)
              ("webkitgtk" ,webkitgtk)))
    (native-inputs `(("desktop-file-utils" ,desktop-file-utils)
                      ;for "update-desktop-database"
                     ("glib:bin" ,glib "bin") ;for "glib-compile-resources"
                     ("gtk+:bin" ,gtk+ "bin") ;for "gtk-update-icon-cache"
                     ("libxml2" ,libxml2)
                     ("pkg-config" ,pkg-config)
                     ("vala" ,vala)))
    (home-page "https://tkashkin.tk/projects/gamehub ")
    (synopsis "Unified library for all your games")
    (description
     "GameHub is a unified library for all your games.  It allows
you to store your games from different platforms into one program
to make it easier for you to manage your games.")
    (license license:gpl3)))
