;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2021-2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix packages minikube)
  #:use-module (ice-9 match)
  #:use-module (guix download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages gcc)
  #:use-module (nonguix build-system binary))

(define-public minikube
  (package
    (name "minikube")
    (version "1.28.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://storage.googleapis.com/minikube/releases/v"
                    version "/minikube-linux-amd64"))
              (sha256
               (base32
                "13idkr74wi43jxfbl7ly7lvwzy454dxs0lz29cfsc5cqrrgavkjq"))))
    (build-system binary-build-system)
    (arguments
     `(#:install-plan `(("minikube" "bin/"))
       #:patchelf-plan `(("./minikube" ("gcc")))
       #:phases (modify-phases %standard-phases
                  (replace 'unpack
                    (lambda* (#:key inputs #:allow-other-keys)
                      (copy-file (assoc-ref inputs "source") "./minikube")
                      (chmod "minikube" #o644)))
                  (add-after 'patchelf 'chmod
                    (lambda _
                      (chmod "minikube" #o555))))))
    (inputs
     (list `(,gcc "lib")))
    (home-page "https://github.com/kubernetes/minikube")
    (supported-systems '("x86_64-linux"))
    (synopsis "Run Kubernetes locally")
    (description
     "minikube implements a local Kubernetes cluster on macOS, Linux,
and Windows.  minikube's primary goals are to be the best tool for local
Kubernetes application development and to support all Kubernetes features that
fit.")
    (license license:expat)))
