;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix packages scripts)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages python)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 popen)
  #:use-module (small-guix self)
  #:use-module (small-guix utils))

(define-public small-guix-scripts
  (make-scripts-package "small-guix-scripts"
   %small-guix-scripts-dir
   (list bash coreutils)
   "A set of utility scripts"
   "This package provides some utility scripts mostly for handling Guix profiles."
   "https://gitlab.com/orang3/small-guix"
   license:gpl3+))
