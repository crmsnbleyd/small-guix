;;; Copyright © 2019 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix packages pyesn)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (guix utils))

(define-public python-pyesn
  (let ((version "0.0.0")
        (revision "0")
        (commit "de294b637450abc072d1e0f49fcca7355450482d"))
    (package
      (name "python-pyesn")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/cknd/pyESN.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0v5f62fn7gbrskfis657g3db3hy6cvh9ngq75jkzhba6m55qddf9"))))
      (build-system trivial-build-system)
      (native-inputs `(("python" ,python)))
      (propagated-inputs `(("python-numpy" ,python-numpy)))
      (arguments
       '(#:modules ((guix build utils))
         #:builder (begin
                     (use-modules (guix build utils))
                     (let* ((source (assoc-ref %build-inputs "source"))
                            (python (string-append (assoc-ref %build-inputs
                                                              "python")
                                                   "/bin/python3"))
                            (out (string-append (assoc-ref %outputs "out")
                                  "/lib/python3.7/site-packages")))
                       (copy-file (string-append source "/testing.py")
                                  "./testing.py")
                       (copy-file (string-append source "/pyESN.py")
                                  "./pyESN.py")
                       ;; (invoke python "testing.py")
                       (mkdir-p out)
                       (copy-file "pyESN.py"
                                  (string-append out "/pyESN.py"))
                       #t))))
      (home-page "https://github.com/cknd/pyESN")
      (synopsis "Python ESN implementation")
      (description "Python ESN implementation.")
      (license license:expat))))
