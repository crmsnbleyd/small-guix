;;; Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix packages scheme-lsp)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages guile-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix utils))

(define-public guile-lsp-server
  (package
    (name "guile-lsp-server")
    (version "0.4.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://codeberg.org/rgherdt/scheme-lsp-server")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "04fc76pnl8mrwrjw49xhzs4r0dx2vh4gy2y151p33hahylf5z1xs"))))
    (build-system gnu-build-system)
    (arguments
     (list #:modules `((ice-9 match)
                       (ice-9 ftw)
                       ,@%gnu-build-system-modules)
           #:phases #~(modify-phases %standard-phases
                        (add-after 'unpack 'move-to-guile-directory
                          (lambda _
                            (chdir "./guile")))
                        (add-after 'install 'wrap-entrypoint
                          (lambda _
                            (let* ((bin (string-append #$output "/bin"))
                                   (site (string-append #$output
                                                        "/share/guile/site"))
                                   (deps (list #$guile-scheme-json-rpc)))
                              (match (scandir site)
                                (("." ".." version)
                                 (let ((modules (string-append site "/"
                                                               version))
                                       (compiled-modules (string-append #$output
                                                          "/lib/guile/"
                                                          version
                                                          "/site-ccache")))
                                   (wrap-program (string-append bin
                                                  "/guile-lsp-server")
                                                 `("GUILE_LOAD_PATH" ":"
                                                   prefix
                                                   (,modules ,@(map (lambda (dep)
                                                                      (string-append
                                                                       dep
                                                                       "/share/guile/site/"
                                                                       version))
                                                                    deps)))
                                                 `("GUILE_LOAD_COMPILED_PATH"
                                                   ":" prefix
                                                   (,compiled-modules ,@(map (lambda 
                                                                                     (dep)
                                                                               
                                                                               (string-append
                                                                                dep
                                                                                "/lib/guile/"
                                                                                version
                                                                                "/site-ccache"))
                                                                         deps))))
                                   #t)))))))))
    (native-inputs (list autoconf automake pkg-config))
    (inputs
     ;; Depend on the latest Guile to avoid bytecode compatibility issues when
     ;; using modules built against the latest version.
     (list guile-3.0-latest))
    (propagated-inputs (list guile-irregex
                             guile-scheme-json-rpc
                             guile-srfi-145
                             guile-srfi-180))
    (synopsis "A LSP (Language Server Protocol) server for Scheme")
    (description
     "@code{guile-scheme-lsp} is an implementation for Guile of the LSP
specification.  This software aims to support several Scheme implementations.
To achieve this, the code is designed to contain as much logic as possible in
R7RS Scheme, separating implementation-specific code in different directories.")
    (home-page "https://codeberg.org/rgherdt/scheme-lsp-server")
    (license license:expat)))

(define-public guile-lsp-server.git
  (let ((version (package-version guile-lsp-server))
        (revision "0")
        (commit "013906985758deab6cc9f40e67888539a128257c"))
    (package
      (inherit guile-lsp-server)
      (name "guile-lsp-server.git")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://codeberg.org/rgherdt/scheme-lsp-server")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1iddkg5342dg3n70zhvvl36vaqxklxv5nlnfwizj0yj46bljhpxn")))))))
