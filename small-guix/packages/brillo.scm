;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix packages brillo)
  #:use-module (ice-9 match)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages golang))

(define-public brillo
  (package
    (name "brillo")
    (version "1.4.10")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/cameronnemo/brillo")
                    (commit (string-append "v" version))))
              (sha256
               (base32
                "0hk99yg0kypwj7sw5gjbiniik216cvq6nshrb6j1zj8fr44bmhn7"))
              (file-name (git-file-name name version))))
    (build-system gnu-build-system)
    (arguments
     `(#:make-flags (list (string-append "CC="
                                         ,(cc-for-target))
                          (string-append "PREFIX=" %output))
       #:phases (modify-phases %standard-phases
                  (delete 'configure)
                  ;; Tests must be run on real hardware.
                  (delete 'check)
                  (add-after 'install 'install-udev-polkit
                    (lambda* (#:key make-flags #:allow-other-keys)
                      (map (lambda (target)
                             (apply invoke "make" target make-flags))
                           '("install.udev" "install.polkit")))))))
    (native-inputs (list go-github-com-go-md2man))
    (home-page "https://gitlab.com/cameronnemo/brillo")
    (synopsis "Controls the brightness of backlight and LED devices on Linux")
    (description
     "Brillo can control the brightness of backlight and LED devices on Linux.

Notable features include:

@itemize

@item Automatic best controller detection
@item Smooth transitions and natural brightness adjustments
@item Ability to save and restore brightness across boots
@item Directly using sysfs to set brightness without relying on X
@item Unprivileged access with no new setuid binaries
@item Containment with AppArmor

@end itemize
")
    (license license:gpl3+)))
