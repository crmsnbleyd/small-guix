;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2021-2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix packages hall)
  #:use-module (gnu packages guile-xyz)
  #:use-module (guix packages)
  #:use-module (guix git-download))

(define-public guile-hall.git
  (let ((commit "c86001fb70becf96a6050577bbdf9cadbb553345")
        (version (package-version guile-hall))
        (revision "0"))
    (package
      (inherit guile-hall)
      (name "guile-hall.git")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/a-sassmannshausen/guile-hall")
                      (commit commit)))
                (file-name (string-append name "-" version "-checkout"))
                (sha256
                 (base32
                  "0psqhr9q4c5awnqprcsn83mdmmynyad1nd2r3vdk072ghlrkgsvh"))))
      (description
       (string-append (package-description guile-hall)
                      "\n\nThis package tracks the master branch.  It currently
provides commit "
                      commit
                      ".")))))
