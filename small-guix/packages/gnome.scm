;;; Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (small-guix packages gnome)
  #:use-module (ice-9 match)
  #:use-module (guix build-system meson)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages python))

(define-public gnome-browser-connector
  (package
    (name "gnome-browser-connector")
    (version "42.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.gnome.org/GNOME/gnome-browser-connector")
                    (commit (string-append "v" version))))
              (sha256
               (base32
                "18hznmhggvwhjgdpm0zckqdhhmwvccp2z3a9awbb4kmrcfwm2h01"))
              (file-name (git-file-name name version))))
    (build-system meson-build-system)
    (arguments
     (list #:glib-or-gtk? #t
           #:imported-modules `((guix build python-build-system)
                                ,@%meson-build-system-modules)
           #:phases #~(modify-phases %standard-phases
                        (add-after 'install 'add-install-to-pythonpath
                         (@@ (guix build python-build-system)
                             add-install-to-pythonpath))
                        (add-after 'add-install-to-pythonpath 'wrap-for-python
                          (@@ (guix build python-build-system) wrap)))))
    (native-inputs (list gobject-introspection))
    (inputs (list python))
    (propagated-inputs
     (list python-pygobject))
    (home-page "https://wiki.gnome.org/Projects/GnomeShellIntegration")
    (synopsis "Native browser connector for integration with extensions.gnome.org")
    (description
     "Native host messaging connector that provides integration with GNOME Shell
and the corresponding extensions repository @url{https://extensions.gnome.org}.")
    (license license:gpl3+)))
