;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix home services mcron)
  #:use-module (guix gexp)
  #:use-module (small-guix packages scripts)
  #:use-module (ice-9 format)
  #:export (cleanup-job))

(define* (cleanup-job #:key (hours 23) (minutes 0))
 ;; Run 'cleanup' at a given hour every day.
 #~(job #$(format #f "~a ~a * * *" minutes hours)
        (string-append #$small-guix-scripts "/bin/cleanup")
        "cleanup"))
