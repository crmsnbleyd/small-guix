;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (small-guix home services env-variables))

(define-public small-guix-environment
  `(("EDITOR" . "emacs")
    ("VISUAL" . "emacs")
    ("XDG_DATA_DIRS" . "${XDG_DATA_DIRS}:${HOME}/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share")
    ("HISTTIMEFORMAT" . "%d/%m/%y %T ")
    ("PATH" . "$HOME/.local/bin:$PATH")
    ("LESSHISTFILE" . "$XDG_CACHE_HOME/.lesshst")
    ("GUIX_EXTRA_PROFILES" . "$HOME/.guix-extra-profiles")
    ("GUIX_MANIFESTS" . "$HOME/.guix-manifests")
    ("_JAVA_AWT_WM_NONREPARENTING" . "1")
    ("RED" . "\\033[0;31m")
    ("GREEN" . "\\033[1;32m")
    ("BLUE" . "\\033[1;34m")
    ("NC" . "\\033[0m")))
