;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2022, 2023 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix home services shell)
  #:use-module (guix gexp)
  #:use-module (gnu home services)
  #:use-module (gnu home services shells)
  #:use-module (small-guix home services const))

(define-public small-guix-shell
  (service home-bash-service-type
           (home-bash-configuration
            (guix-defaults? #t)
            (aliases '(("la" . "ls -la") ("lr" . "ls -ltr")
                       ("nix-update" . "nix-channel --update && nix-env -u")))
            (bash-profile
             (list
              (file-append %dotfiles-dir "/bash/bash_profile_git_branch")))
            (bashrc (list (file-append %dotfiles-dir "/bash/bashrc_tmux")
                          (file-append %dotfiles-dir "/bash/bashrc_direnv"))))))
