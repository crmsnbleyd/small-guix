;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (small-guix home services shell-profile)
  #:use-module (guix gexp)
  #:use-module (gnu services)
  #:use-module (gnu home)
  #:use-module (gnu home services shells)
  #:use-module (small-guix home services const))

(define-public small-guix-shell-profile
  (simple-service 'small-guix-dot-profile home-shell-profile-service-type
                  (list (file-append %dotfiles-dir "/bash/bash_functions")
                        (file-append %dotfiles-dir "/bash/profile_guix_extra")
                        (file-append %dotfiles-dir "/bash/profile_nix"))))
