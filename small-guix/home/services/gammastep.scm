;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (small-guix home services gammastep)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu packages xdisorg)

  #:export (gammastep-configuration gammastep-configuration?

                                    gammastep-service-type))

;;; Commentary:
;;;
;;; gammastep service.
;;;
;;; This comes mostly from
;;; https://guix.gnu.org/manual/devel/en/guix.html#Complex-Configurations
;;;
;;; Code:
;;;
;;;
;;;   -v            Increase logging verbosity
;; -q            Decrease logging verbosity
;; -V            Show program version

;; -b DAY:NIGHT  Screen brightness to apply (between 0.1 and 1.0)
;; -c FILE       Load settings from specified configuration file
;; -g R:G:B      Additional gamma correction to apply
;; -l PROVIDER   Select provider for automatic location updates
;;               (Type `list' to see available providers)
;; -m METHOD     Method to use to set color temperature
;;               (Type `list' to see available methods)
;; -o            One shot mode (do not continuously adjust color temperature)
;; -O TEMP       One shot manual mode (set color temperature)
;; -p            Print mode (only print parameters and exit)
;; -P            Reset existing gamma ramps before applying new color effect
;; -x            Reset mode (remove adjustment from screen)
;; -r            Disable fading between color temperatures
;; -t DAY:NIGHT  Color temperature to set at daytime/night)
;;;
;;;
;;;

(define-configuration/no-serialization gammastep-configuration
                                       (gammastep (package
                                                    gammastep)
                                        "The @code{gammastep} package to use.")
                                       (coordinates (string "0.0:0.0")
                                                    "Your current location."))

(define (gammastep-shepherd-service config)
  (let ((gammastep (gammastep-configuration-gammastep config))
        (coordinates (gammastep-configuration-coordinates config)))
    (shepherd-service (provision '(gammastep))
                      (respawn? #t)
                      (auto-start? #f)
                      (start #~(make-forkexec-constructor (list #$(file-append
                                                                   gammastep
                                                                   "/bin/gammastep")
                                                                "-l"
                                                                #$coordinates)
                                                          #:log-file (string-append
                                                                      (or (getenv
                                                                           "XDG_LOG_HOME")
                                                                          (format
                                                                           #f
                                                                           "~a/.local/var/log"
                                                                           (getenv
                                                                            "HOME")))
                                                                      "/gammastep.log")))
                      (stop #~(make-kill-destructor)))))

(define gammastep-service-type
  (service-type (name 'gammastep)
                (extensions (list (service-extension
                                   home-shepherd-service-type
                                   (compose list gammastep-shepherd-service))
                                  (service-extension home-profile-service-type
                                   (compose list
                                            gammastep-configuration-gammastep))))
                (default-value (gammastep-configuration))
                (description
                 "Provides @code{gammastep} Shepherd service and installs
@code{gammastep} in Guix Home's profile.")))
