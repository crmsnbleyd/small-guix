;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (small-guix home services dotfiles)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (gnu services)
  #:use-module (gnu home services)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 ftw))

(define (dir-contents dir)
  (scandir dir
           (lambda (name)
             (not (member name
                          '("." ".."))))))

(define (list-files-recursively dir)
  (let ((cwd (getcwd)))
    (chdir dir)
    (let ((files (map (lambda (f)
                        (string-drop f 2))
                      (find-files "."))))
      (chdir cwd) files)))

(define (generate-dotfiles dir store-output-path dotfiles-dir-path)
  (map (lambda (path)
         (list path
               (file-append store-output-path
                            (string-append "/" dir "/" path))))
       (list-files-recursively (string-append dotfiles-dir-path "/" dir))))

(define-public (small-guix-dotfiles store-output-path dotfiles-dir-path)
  (simple-service 'small-guix-dotfiles home-files-service-type
                  (fold append
                        '()
                        (map (lambda (dir)
                               (generate-dotfiles dir store-output-path
                                                  dotfiles-dir-path))
                             (dir-contents dotfiles-dir-path)))))
