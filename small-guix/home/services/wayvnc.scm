;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (small-guix home services wayvnc)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (small-guix packages wayvnc)
  #:use-module (ice-9 string-fun)
  #:use-module (srfi srfi-1)

  #:export (wayvnc-configuration wayvnc-configuration?
                                 wayvnc-configuration-wayvnc
                                 wayvnc-configuration-address

                                 wayvnc-service-type))

;;; Commentary:
;;;
;;; wayvnc service.
;;;
;;; This comes mostly from
;;; https://guix.gnu.org/manual/devel/en/guix.html#Complex-Configurations
;;;
;;; Code:

;; Turn field names, which are Scheme symbols into strings
(define (uglify-field-name field-name)
  (let ((str (symbol->string field-name)))
    (string-replace-substring (if (string-suffix? "?" str)
                                  (string-drop-right str 1) str) "-" "_")))

(define (wayvnc-serialize-string field-name value)
  #~(string-append #$(uglify-field-name field-name) "="
                   #$value "\n"))

(define (wayvnc-serialize-boolean field-name value)
  (serialize-string field-name
                    (if value "true" "false")))

(define wayvnc-serialize-package
  empty-serializer)

(define (serialize-wayvnc-configurations field-name value)
  #~(string-append #$@(map (cut serialize-configuration <>
                                wayvnc-configuration-fields) value)))

(define-maybe string
              (prefix wayvnc-))

(define-configuration wayvnc-configuration
                      (wayvnc (package
                                wayvnc) "The @code{wayvnc} package to use.")
                      (address (string "0.0.0.0")
                               "The IP address to listen upon.")
                      (enable-auth? (boolean #f)
                                    "Whether to enable authentication.")
                      (username maybe-string
                                "The username used for the authentication.")
                      (password maybe-string
                                "The password used for the authentication.")
                      (private-key-file maybe-string
                                        "The SSL private key file.")
                      (certificate-file maybe-string
                                        "The SSL certificate chain file.")
                      (prefix wayvnc-))

(define (wayvnc-shepherd-service config)
  (let ((wayvnc (wayvnc-configuration-wayvnc config)))
    (shepherd-service (provision '(wayvnc))
                      (start #~(make-forkexec-constructor (list #$(file-append
                                                                   wayvnc
                                                                   "/bin/wayvnc")
                                                                "--config"
                                                                #$config)
                                                          #:log-file (string-append
                                                                      (or (getenv
                                                                           "XDG_LOG_HOME")
                                                                          (format
                                                                           #f
                                                                           "~a/.local/var/log"
                                                                           (getenv
                                                                            "HOME")))
                                                                      "/wayvnc.log")))
                      (stop #~(make-kill-destructor)))))

(define wayvnc-service-type
  (service-type (name 'wayvnc)
                (extensions (list (service-extension
                                   home-shepherd-service-type
                                   (compose list wayvnc-shepherd-service))
                                  (service-extension home-profile-service-type
                                   (compose list wayvnc-configuration-wayvnc))))
                (default-value (wayvnc-configuration))
                (description
                 "Provides @code{wayvnc} Shepherd service and installs
@code{wayvnc} in Guix Home's profile.")))
