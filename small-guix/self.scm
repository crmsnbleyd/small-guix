;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2022 Giacomo Leidi <goodoldpaul@autistici.org>

(define-module (small-guix self)
  #:use-module (guix channels)
  #:use-module (guix gexp))

(define-public %small-guix-config-dir
  (local-file ".." "small-guix-config-dir"
              #:recursive? #t))

(define-public %small-guix-scripts-dir
  (local-file "../bin" "small-guix-scripts-dir"
              #:recursive? #t))

(define-public %small-guix-channels
  (append %default-channels
          (list (channel (name 'small-guix)
                         (url "https://gitlab.com/orang3/small-guix.git")))))
